/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2023 SoftAtHome
**
** Redistribution and use in source and binary forms, with or without modification,
** are permitted provided that the following conditions are met:
**
** 1. Redistributions of source code must retain the above copyright notice,
** this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above copyright notice,
** this list of conditions and the following disclaimer in the documentation
** and/or other materials provided with the distribution.
**
** Subject to the terms and conditions of this license, each copyright holder
** and contributor hereby grants to those receiving rights under this license
** a perpetual, worldwide, non-exclusive, no-charge, royalty-free, irrevocable
** (except for failure to satisfy the conditions of this license) patent license
** to make, have made, use, offer to sell, sell, import, and otherwise transfer
** this software, where such license applies only to those patent claims, already
** acquired or hereafter acquired, licensable by such copyright holder or contributor
** that are necessarily infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright holders and
** non-copyrightable additions of contributors, in source or binary form) alone;
** or
**
** (b) combination of their Contribution(s) with the work of authorship to which
** such Contribution(s) was added by such copyright holder or contributor, if,
** at the time the Contribution is added, such addition causes such combination
** to be necessarily infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any copyright
** holder or contributor is granted under this license, whether expressly, by
** implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
** AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
** IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
** ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
** LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
** DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
** SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
** CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
** OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
** USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include <stdlib.h>
#include <stdio.h>
#include <setjmp.h>
#include <stdarg.h>
#include <cmocka.h>
#include <test_common.h>
#include "mock.h"
#include "firewall.h"

static amxd_dm_t dm;
static amxo_parser_t parser;

amxo_parser_t* mock_get_parser(void) {
    return &parser;
}

static amxp_expr_status_t mock_mqttbroker_search_path(UNUSED amxp_expr_t* expr,
                                                      amxc_var_t* args,
                                                      amxc_var_t* ret) {
    amxp_expr_status_t status = amxp_expr_status_ok;
    amxc_llist_it_t* it = amxc_llist_get_first(&args->data.vl);
    amxc_llist_t paths;
    amxc_var_t* data = amxc_var_from_llist_it(it);
    const char* path = amxc_var_constcast(cstring_t, data);

    amxc_llist_init(&paths);
    amxd_dm_resolve_pathf(&dm, &paths, "%s", path);

    amxc_var_set_type(ret, AMXC_VAR_ID_LIST);
    amxc_llist_for_each(it, (&paths)) {
        amxc_string_t* p = amxc_string_from_llist_it(it);
        amxc_var_add(cstring_t, ret, amxc_string_get(p, 0));
    }

    amxc_llist_clean(&paths, amxc_string_list_it_free);

    return status;
}

amxd_dm_t* mock_mqttbroker_init_dm(void) {
    assert_int_equal(amxd_dm_init(&dm), amxd_status_ok);
    return &dm;
}

amxd_dm_t* mock_mqttbroker_get_dm(void) {
    return &dm;
}

void mock_mqttbroker_cleanup_dm(void) {
    handle_events();

    amxo_resolver_import_close_all();
    amxo_parser_clean(&parser);
    amxd_dm_clean(&dm);
}

CONSTRUCTOR static void mock_mqttbroker_expr_functions_init(void) {
    amxp_expr_add_value_fn("search_path", mock_mqttbroker_search_path);
}

int __wrap_amxm_execute_function(UNUSED const char* const shared_object_name,
                                 UNUSED const char* const module_name,
                                 UNUSED const char* const func_name,
                                 UNUSED amxc_var_t* args,
                                 UNUSED amxc_var_t* ret) {
    return mock();
}
