/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2023 SoftAtHome
**
** Redistribution and use in source and binary forms, with or without modification,
** are permitted provided that the following conditions are met:
**
** 1. Redistributions of source code must retain the above copyright notice,
** this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above copyright notice,
** this list of conditions and the following disclaimer in the documentation
** and/or other materials provided with the distribution.
**
** Subject to the terms and conditions of this license, each copyright holder
** and contributor hereby grants to those receiving rights under this license
** a perpetual, worldwide, non-exclusive, no-charge, royalty-free, irrevocable
** (except for failure to satisfy the conditions of this license) patent license
** to make, have made, use, offer to sell, sell, import, and otherwise transfer
** this software, where such license applies only to those patent claims, already
** acquired or hereafter acquired, licensable by such copyright holder or contributor
** that are necessarily infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright holders and
** non-copyrightable additions of contributors, in source or binary form) alone;
** or
**
** (b) combination of their Contribution(s) with the work of authorship to which
** such Contribution(s) was added by such copyright holder or contributor, if,
** at the time the Contribution is added, such addition causes such combination
** to be necessarily infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any copyright
** holder or contributor is granted under this license, whether expressly, by
** implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
** AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
** IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
** ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
** LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
** DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
** SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
** CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
** OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
** USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/
#include <stdlib.h>
#include <stdio.h>
#include <setjmp.h>
#include <stdarg.h>
#include <cmocka.h>

#include "mqttbroker.h"
#include "dm_mqttbroker.h"
#include "interface.h"

#include "mock.h"
#include "test_mqttbroker_functions.h"
#include "test_common.h"

static const char* odl_defs = "../../odl/tr181-mqttbroker_definition.odl";

static amxo_parser_t parser;
amxd_object_t* dev_obj = NULL;

int test_mqttbroker_setup(UNUSED void** state) {
    amxd_dm_t* dm = mock_mqttbroker_init_dm();
    amxd_object_t* root_obj = NULL;

    assert_int_equal(amxo_parser_init(&parser), 0);

    root_obj = amxd_dm_get_root(dm);
    assert_non_null(root_obj);

    amxo_resolver_ftab_add(&parser, "mqtt_broker_start", AMXO_FUNC(_mqtt_broker_start));
    amxo_resolver_ftab_add(&parser, "mqtt_broker_toggled", AMXO_FUNC(_mqtt_broker_toggled));
    amxo_resolver_ftab_add(&parser, "mqtt_broker_updated", AMXO_FUNC(_mqtt_broker_updated));
    amxo_resolver_ftab_add(&parser, "mqtt_broker_added", AMXO_FUNC(_mqtt_broker_added));
    amxo_resolver_ftab_add(&parser, "mqtt_broker_removed", AMXO_FUNC(_mqtt_broker_removed));
    amxo_resolver_ftab_add(&parser, "mqtt_broker_cleanup", AMXO_FUNC(_mqtt_broker_cleanup));
    amxo_resolver_ftab_add(&parser, "mqtt_broker_auth_changed", AMXO_FUNC(_mqtt_broker_auth_changed));
    amxo_resolver_ftab_add(&parser, "mqtt_broker_acl_changed", AMXO_FUNC(_mqtt_broker_acl_changed));
    amxo_resolver_ftab_add(&parser, "mqtt_broker_user_changed", AMXO_FUNC(_mqtt_broker_user_changed));
    amxo_resolver_ftab_add(&parser, "addBroker", AMXO_FUNC(_addBroker));
    amxo_resolver_ftab_add(&parser, "deleteBroker", AMXO_FUNC(_deleteBroker));
    amxo_resolver_ftab_add(&parser, "getBroker", AMXO_FUNC(_getBroker));
    amxo_resolver_ftab_add(&parser, "setEnable", AMXO_FUNC(_setEnable));

    assert_int_equal(amxo_parser_parse_file(&parser, odl_defs, root_obj), 0);
    handle_events();

    dev_obj = amxd_object_findf(&dm->object, "MQTTBroker.");

    return 0;
}

int test_mqttbroker_teardown(UNUSED void** state) {
    amxd_dm_t* dm = mock_mqttbroker_get_dm();

    assert_int_equal(_mqttbroker_main(1, dm, &parser), 0);
    amxo_parser_clean(&parser);
    mock_mqttbroker_cleanup_dm();

    return 0;
}

void test_add_broker(UNUSED void** state) {
    char* alias = "broker_id1";
    char* name = "broker_name1";
    char* status = NULL;
    amxc_var_t args;
    amxc_var_t broker;
    amxc_var_t ret;
    amxd_object_t* new_broker = NULL;

    amxc_var_init(&args);
    amxc_var_init(&broker);
    amxc_var_init(&ret);

    amxc_var_set_type(&args, AMXC_VAR_ID_HTABLE);
    amxc_var_set_type(&broker, AMXC_VAR_ID_HTABLE);

    amxc_var_add_key(cstring_t, &args, "alias", alias);

    amxc_var_add_key(cstring_t, &broker, "Name", name);
    amxc_var_add_key(bool, &broker, "Enable", false);
    amxc_var_add_key(cstring_t, &broker, "Interface", "lo");
    amxc_var_add_key(uint32_t, &broker, "Port", 1888);
    amxc_var_add_key(amxc_htable_t, &args, "broker", amxc_var_constcast(amxc_htable_t, &broker));

    assert_int_equal(amxd_object_invoke_function(dev_obj,
                                                 "addBroker",
                                                 &args,
                                                 &ret),
                     amxd_status_ok);

    new_broker = amxd_object_findf(dev_obj, "Broker.%s.", alias);
    assert_non_null(new_broker);

    status = amxd_object_get_value(cstring_t, new_broker, "Status", NULL);
    assert_string_equal(status, "Disabled");

    free(status);
    amxc_var_clean(&args);
    amxc_var_clean(&broker);
    amxc_var_clean(&ret);
}

void test_get_broker(UNUSED void** state) {
    char* alias = "broker_id1";
    char* name = "broker_name1";
    amxc_var_t args;
    amxc_var_t ret;

    amxc_var_init(&args);
    amxc_var_init(&ret);

    amxc_var_set_type(&args, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(cstring_t, &args, "alias", alias);

    assert_int_equal(amxd_object_invoke_function(dev_obj,
                                                 "getBroker",
                                                 &args,
                                                 &ret),
                     amxd_status_ok);

    amxc_var_dump(&ret, 1);
    assert_string_equal(GET_CHAR(&ret, "Name"), name);

    amxc_var_clean(&args);
    amxc_var_clean(&ret);
}

void test_set_enable(UNUSED void** state) {
    char* alias = "broker_id1";
    UNUSED char* status = NULL;
    amxc_var_t args;
    amxc_var_t ret;
    amxd_object_t* broker = NULL;

    amxc_var_init(&args);
    amxc_var_init(&ret);

    broker = amxd_object_findf(dev_obj, "Broker.%s.", alias);
    assert_non_null(broker);

    amxc_var_set_type(&args, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(cstring_t, &args, "alias", alias);
    amxc_var_add_key(bool, &args, "enable", true);

    assert_int_equal(amxd_object_invoke_function(dev_obj,
                                                 "setEnable",
                                                 &args,
                                                 &ret),
                     amxd_status_ok);

    broker = amxd_object_findf(dev_obj, "Broker.%s.", alias);
    assert_non_null(broker);

    bool enabled = amxd_object_get_value(bool, broker, "Enable", NULL);
    assert_true(enabled);

    status = amxd_object_get_value(cstring_t, broker, "Status", NULL);
    assert_string_equal(status, "Enabled");

    free(status);
    amxc_var_clean(&args);
    amxc_var_clean(&ret);
}

void test_delete_broker(UNUSED void** state) {
    char* alias = "broker_id1";
    amxc_var_t args;
    amxc_var_t ret;

    amxc_var_init(&args);
    amxc_var_init(&ret);

    amxc_var_set_type(&args, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(cstring_t, &args, "alias", alias);

    assert_non_null(amxd_object_findf(dev_obj, "Broker.%s.", alias));

    assert_int_equal(amxd_object_invoke_function(dev_obj,
                                                 "deleteBroker",
                                                 &args,
                                                 &ret),
                     amxd_status_ok);

    assert_null(amxd_object_findf(dev_obj, "Broker.%s.", alias));

    amxc_var_clean(&args);
    amxc_var_clean(&ret);
}

void test_can_start_plugin(UNUSED void** state) {
    assert_int_equal(_mqttbroker_main(0, mock_mqttbroker_get_dm(), &parser), 0);
}

void test_can_stop_plugin(UNUSED void** state) {
    assert_int_equal(_mqttbroker_main(1, mock_mqttbroker_get_dm(), &parser), 0);
}
