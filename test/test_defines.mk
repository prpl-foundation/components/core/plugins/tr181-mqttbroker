MACHINE = $(shell $(CC) -dumpmachine)

SRCDIR = $(realpath ../../src)
OBJDIR = $(realpath ../../output/$(MACHINE)/coverage)
INCDIR = $(realpath ../../include ../../include_priv ../include)
COMMON_SRC_DIR = ../test_common

HEADERS = $(wildcard $(INCDIR)/$(TARGET)/*.h)
SOURCES = $(wildcard $(SRCDIR)/*.c) $(wildcard $(COMMON_SRC_DIR)/*.c)

MOCK_WRAP =	amxb_listen \
			amxb_accept \
			amxb_get_fd \
			amxb_free \
			amxb_read_raw \
			netmodel_initialize \
			netmodel_cleanup \
			netmodel_openQuery_isUp \
			netmodel_closeQuery \
			netmodel_getFirstParameter \
			amxm_execute_function \

WRAP_FUNC=-Wl,--wrap=

CFLAGS += -Werror -Wall -Wextra -Wno-attributes\
          --std=gnu99 -g3 -Wmissing-declarations \
		  $(addprefix -I ,$(INCDIR)) -I$(OBJDIR)/.. -I../mocks -I../test_common \
		  -fkeep-inline-functions -fkeep-static-functions -Wno-format-nonliteral \
		  $(shell pkg-config --cflags cmocka) -pthread

LDFLAGS += -fkeep-inline-functions -fkeep-static-functions \
		   $(shell pkg-config --libs cmocka) -lamxc -lamxj -lamxp -lamxd -lamxb -lamxo -lamxm \
		   -ldl -lpthread -limtp -lsahtrace -lm 

LDFLAGS += -g $(addprefix $(WRAP_FUNC),$(MOCK_WRAP))
