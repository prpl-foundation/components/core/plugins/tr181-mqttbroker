/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2023 SoftAtHome
**
** Redistribution and use in source and binary forms, with or without modification,
** are permitted provided that the following conditions are met:
**
** 1. Redistributions of source code must retain the above copyright notice,
** this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above copyright notice,
** this list of conditions and the following disclaimer in the documentation
** and/or other materials provided with the distribution.
**
** Subject to the terms and conditions of this license, each copyright holder
** and contributor hereby grants to those receiving rights under this license
** a perpetual, worldwide, non-exclusive, no-charge, royalty-free, irrevocable
** (except for failure to satisfy the conditions of this license) patent license
** to make, have made, use, offer to sell, sell, import, and otherwise transfer
** this software, where such license applies only to those patent claims, already
** acquired or hereafter acquired, licensable by such copyright holder or contributor
** that are necessarily infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright holders and
** non-copyrightable additions of contributors, in source or binary form) alone;
** or
**
** (b) combination of their Contribution(s) with the work of authorship to which
** such Contribution(s) was added by such copyright holder or contributor, if,
** at the time the Contribution is added, such addition causes such combination
** to be necessarily infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any copyright
** holder or contributor is granted under this license, whether expressly, by
** implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
** AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
** IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
** ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
** LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
** DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
** SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
** CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
** OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
** USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/
#include <stdlib.h>
#include <stdio.h>
#include <setjmp.h>
#include <stdarg.h>
#include <cmocka.h>

#include "mqttbroker.h"
#include "dm_mqttbroker.h"
#include "interface.h"

#include "mock.h"
#include "test_mqttbroker_enable_disable_interface.h"
#include "test_common.h"

static char* _assert_str;
#define assert_str_attr_equal(o, a, e) \
    _assert_str = amxd_object_get_value(cstring_t, o, a, NULL); \
    _assert_string_equal((_assert_str), (e), __FILE__, __LINE__); \
    free(_assert_str);

static const char* odl_defs = "../../odl/tr181-mqttbroker_definition.odl";

static amxo_parser_t parser;

static void* priv = NULL;

int test_mqttbroker_setup(UNUSED void** state) {
    amxd_dm_t* dm = mock_mqttbroker_init_dm();
    amxd_object_t* root_obj = NULL;

    assert_int_equal(amxo_parser_init(&parser), 0);

    root_obj = amxd_dm_get_root(dm);
    assert_non_null(root_obj);

    amxo_resolver_ftab_add(&parser, "mqtt_broker_start", AMXO_FUNC(_mqtt_broker_start));
    amxo_resolver_ftab_add(&parser, "mqtt_broker_toggled", AMXO_FUNC(_mqtt_broker_toggled));
    amxo_resolver_ftab_add(&parser, "mqtt_broker_updated", AMXO_FUNC(_mqtt_broker_updated));
    amxo_resolver_ftab_add(&parser, "mqtt_broker_added", AMXO_FUNC(_mqtt_broker_added));
    amxo_resolver_ftab_add(&parser, "mqtt_broker_removed", AMXO_FUNC(_mqtt_broker_removed));
    amxo_resolver_ftab_add(&parser, "mqtt_broker_cleanup", AMXO_FUNC(_mqtt_broker_cleanup));
    amxo_resolver_ftab_add(&parser, "mqtt_broker_auth_changed", AMXO_FUNC(_mqtt_broker_auth_changed));
    amxo_resolver_ftab_add(&parser, "mqtt_broker_acl_changed", AMXO_FUNC(_mqtt_broker_acl_changed));
    amxo_resolver_ftab_add(&parser, "mqtt_broker_user_changed", AMXO_FUNC(_mqtt_broker_user_changed));
    amxo_resolver_ftab_add(&parser, "addBroker", AMXO_FUNC(_addBroker));
    amxo_resolver_ftab_add(&parser, "deleteBroker", AMXO_FUNC(_deleteBroker));
    amxo_resolver_ftab_add(&parser, "getBroker", AMXO_FUNC(_getBroker));

    assert_int_equal(amxo_parser_parse_file(&parser, odl_defs, root_obj), 0);
    handle_events();

    return 0;
}

int test_mqttbroker_teardown(UNUSED void** state) {
    amxd_dm_t* dm = mock_mqttbroker_get_dm();

    assert_int_equal(_mqttbroker_main(1, dm, &parser), 0);
    amxo_parser_clean(&parser);
    mock_mqttbroker_cleanup_dm();

    return 0;
}

void test_broker_dm_create(UNUSED void** state) {
    amxd_dm_t* dm = mock_mqttbroker_get_dm();
    amxd_trans_t trans;
    amxc_var_t params;
    amxd_object_t* object = NULL;

    amxd_trans_init(&trans);
    amxc_var_init(&params);

    amxd_trans_select_pathf(&trans, "MQTTBroker.Broker");
    amxd_trans_add_inst(&trans, 0, "DummyBroker");
    amxd_trans_set_value(uint32_t, &trans, "Port", 1885);
    amxd_trans_set_value(uint32_t, &trans, "BrokerID", 99);
    assert_int_equal(amxd_trans_apply(&trans, dm), 0);
    handle_events();
    object = amxd_dm_findf(mock_mqttbroker_get_dm(), "MQTTBroker.Broker.1");
    assert_non_null(object);
    assert_int_equal(amxd_object_get_params(object, &params, amxd_dm_access_protected), 0);
    amxc_var_dump(&params, STDOUT_FILENO);
    assert_string_equal(GET_CHAR(&params, "Status"), "Disabled");

    priv = object->priv;

    amxd_trans_clean(&trans);
    amxc_var_clean(&params);
}

void test_broker_dm_delete(UNUSED void** state) {
    amxd_dm_t* dm = mock_mqttbroker_get_dm();
    amxd_trans_t trans;

    amxd_trans_init(&trans);

    amxd_trans_select_pathf(&trans, "MQTTBroker.Broker");
    amxd_trans_del_inst(&trans, 0, "DummyBroker");
    assert_int_equal(amxd_trans_apply(&trans, dm), 0);
    handle_events();

    amxd_trans_clean(&trans);
}

void test_when_fw_down_broker_error(UNUSED void** state) {
    amxd_dm_t* dm = mock_mqttbroker_get_dm();
    amxd_trans_t trans;
    amxc_var_t params;
    amxd_object_t* object = NULL;

    amxd_trans_init(&trans);
    amxc_var_init(&params);

    will_return(__wrap_amxm_execute_function, -1);

    object = amxd_dm_findf(mock_mqttbroker_get_dm(), "MQTTBroker.Broker.1");
    amxd_trans_select_object(&trans, object);
    amxd_trans_set_value(bool, &trans, "Enable", true);
    amxd_trans_set_value(cstring_t, &trans, "Name", "DummyBroker");
    amxd_trans_set_value(cstring_t, &trans, "Interface", "Device.IP.Interface.1.");
    assert_int_equal(amxd_trans_apply(&trans, dm), 0);
    handle_events();
    assert_int_equal(amxd_object_get_params(object, &params, amxd_dm_access_protected), 0);
    amxc_var_dump(&params, STDOUT_FILENO);
    assert_string_equal(GET_CHAR(&params, "Status"), "Error");

    amxd_trans_clean(&trans);
    amxc_var_clean(&params);
}

void test_when_fw_up_broker_enabled(UNUSED void** state) {
    amxd_dm_t* dm = mock_mqttbroker_get_dm();
    amxd_trans_t trans;
    amxc_var_t params;
    amxd_object_t* object = NULL;

    amxd_trans_init(&trans);
    amxc_var_init(&params);

    will_return(__wrap_amxm_execute_function, 0);

    object = amxd_dm_findf(mock_mqttbroker_get_dm(), "MQTTBroker.Broker.1");
    amxd_trans_select_object(&trans, object);
    amxd_trans_set_value(bool, &trans, "Enable", true);
    amxd_trans_set_value(cstring_t, &trans, "Name", "dummyBroker");
    amxd_trans_set_value(cstring_t, &trans, "Interface", "Device.IP.Interface.1.");
    assert_int_equal(amxd_trans_apply(&trans, dm), 0);
    handle_events();
    assert_int_equal(amxd_object_get_params(object, &params, amxd_dm_access_protected), 0);
    amxc_var_dump(&params, STDOUT_FILENO);
    assert_string_equal(GET_CHAR(&params, "Status"), "Enabled");

    amxd_trans_clean(&trans);
    amxc_var_clean(&params);
}

void test_broker_disable(UNUSED void** state) {
    amxd_dm_t* dm = mock_mqttbroker_get_dm();
    amxd_trans_t trans;
    amxc_var_t params;
    amxc_var_t data;
    amxd_object_t* object = NULL;

    amxc_var_init(&data);
    amxd_trans_init(&trans);
    amxc_var_init(&params);

    will_return(__wrap_amxm_execute_function, 0);

    amxc_var_set_bool(&data, true);
    object = amxd_dm_findf(mock_mqttbroker_get_dm(), "MQTTBroker.Broker.1");

    amxd_trans_select_object(&trans, object);
    amxd_trans_set_value(bool, &trans, "Enable", false);
    amxd_trans_set_value(cstring_t, &trans, "Name", "dummyBroker");
    amxd_trans_set_value(cstring_t, &trans, "Interface", "Device.IP.Interface.1.");
    assert_int_equal(amxd_trans_apply(&trans, dm), 0);

    handle_events();

    assert_int_equal(amxd_object_get_params(object, &params, amxd_dm_access_protected), 0);
    amxc_var_dump(&params, STDOUT_FILENO);
    assert_string_equal(GET_CHAR(&params, "Status"), "Disabled");

    amxc_var_clean(&data);
    amxd_trans_clean(&trans);
    amxc_var_clean(&params);
}

void test_passwdfile_created(UNUSED void** state) {
    amxd_dm_t* dm = mock_mqttbroker_get_dm();
    amxd_object_t* testUser = NULL;
    amxd_trans_t transaction;
    amxc_string_t str;
    size_t len = 0;
    ssize_t read = 0;
    bool found = false;
    char* line = NULL;

    will_return(__wrap_amxm_execute_function, 0);
    will_return(__wrap_amxm_execute_function, 0);

    amxd_trans_init(&transaction);
    amxd_trans_select_pathf(&transaction, "MQTTBroker.Broker.1.User.");
    amxd_trans_add_inst(&transaction, 0, "test");
    amxd_trans_set_value(cstring_t, &transaction, "Username", "testUsername");
    amxd_trans_set_value(cstring_t, &transaction, "Password", "testPassword");
    assert_int_equal(amxd_trans_apply(&transaction, dm), 0);
    amxd_trans_clean(&transaction);
    handle_events();

    testUser = amxd_dm_findf(dm, "MQTTBroker.Broker.1.User.1.");
    assert_non_null(testUser);
    assert_str_attr_equal(testUser, "Username", "testUsername");

    FILE* file = fopen("/var/mqttbroker/broker_99_mosquitto.conf", "r");
    assert_non_null(file);

    amxc_string_init(&str, 0);

    while((read = getline(&line, &len, file)) != -1) {
        if(read > 0) {
            amxc_string_set(&str, line);
            if(amxc_string_search(&str, "password_file /var/mqttbroker/DummyBroker_mosquitto.pass", 0) != -1) {
                found = true;
                break;
            }
            amxc_string_reset(&str);
        }
    }

    will_return(__wrap_amxm_execute_function, 0);
    will_return(__wrap_amxm_execute_function, 0);

    amxd_trans_init(&transaction);
    amxd_trans_select_pathf(&transaction, "MQTTBroker.Broker.1.User.1.");
    amxd_trans_set_value(cstring_t, &transaction, "Username", "testUsernameAgain");
    amxd_trans_set_value(cstring_t, &transaction, "Password", "testPasswordAgain");
    assert_int_equal(amxd_trans_apply(&transaction, dm), 0);
    amxd_trans_clean(&transaction);
    handle_events();

    amxc_string_clean(&str);
    free(line);
    fclose(file);
    assert_true(found);

    file = fopen("/var/mqttbroker/DummyBroker_mosquitto.pass", "r");
    assert_non_null(file);
    fclose(file);
}

void test_aclfile_created(UNUSED void** state) {
    amxd_dm_t* dm = mock_mqttbroker_get_dm();
    amxd_object_t* testACL = NULL;
    amxd_trans_t transaction;
    amxc_string_t str;
    amxc_var_t params;
    size_t len = 0;
    ssize_t read = 0;
    bool foundTopic = false;
    bool foundUser = false;
    bool foundEntry = false;
    char* line = NULL;
    FILE* file = NULL;

    will_return(__wrap_amxm_execute_function, 0);
    will_return(__wrap_amxm_execute_function, 0);

    amxd_trans_init(&transaction);
    amxd_trans_select_pathf(&transaction, "MQTTBroker.Broker.1.User.1.ACL.");
    amxd_trans_add_inst(&transaction, 0, "test");
    amxd_trans_set_value(cstring_t, &transaction, "Topic", "testTopic");
    amxd_trans_set_value(cstring_t, &transaction, "AccessRights", "read");
    assert_int_equal(amxd_trans_apply(&transaction, dm), 0);
    amxd_trans_clean(&transaction);
    handle_events();

    testACL = amxd_dm_findf(dm, "MQTTBroker.Broker.1.User.1.ACL.1.");
    assert_non_null(testACL);
    assert_str_attr_equal(testACL, "Topic", "testTopic");
    assert_str_attr_equal(testACL, "AccessRights", "read");

    amxc_string_init(&str, 0);

    file = fopen("/var/mqttbroker/DummyBroker_mosquitto.acl", "r");
    assert_non_null(file);
    while((read = getline(&line, &len, file)) != -1) {
        if(read > 0) {
            amxc_string_set(&str, line);
            if(amxc_string_search(&str, "user testUsernameAgain", 0) != -1) {
                foundUser = true;
            }
            if(amxc_string_search(&str, "topic read testTopic", 0) != -1) {
                foundTopic = true;
            }

            amxc_string_reset(&str);
        }
    }

    file = fopen("/var/mqttbroker/broker_99_mosquitto.conf", "r");
    assert_non_null(file);
    while((read = getline(&line, &len, file)) != -1) {
        if(read > 0) {
            amxc_string_set(&str, line);
            if(amxc_string_search(&str, "acl_file /var/mqttbroker/DummyBroker_mosquitto.acl", 0) != -1) {
                foundEntry = true;
                break;
            }
            amxc_string_reset(&str);
        }
    }

    assert_true(foundUser);
    assert_true(foundTopic);
    assert_true(foundEntry);

    will_return(__wrap_amxm_execute_function, 0);
    will_return(__wrap_amxm_execute_function, 0);

    amxd_trans_init(&transaction);
    amxd_trans_select_pathf(&transaction, "MQTTBroker.Broker.1.User.1.ACL.1.");
    amxd_trans_set_value(cstring_t, &transaction, "Topic", "testTopic2");
    assert_int_equal(amxd_trans_apply(&transaction, dm), 0);
    amxd_trans_clean(&transaction);
    handle_events();

    amxc_var_init(&params);
    assert_int_equal(amxd_object_get_params(amxd_dm_findf(dm, "MQTTBroker.Broker.1."), &params, amxd_dm_access_protected), 0);
    amxc_var_dump(&params, STDOUT_FILENO);

    assert_str_attr_equal(testACL, "Topic", "testTopic2");

    file = fopen("/var/mqttbroker/DummyBroker_mosquitto.acl", "r");
    foundTopic = false;
    assert_non_null(file);
    while((read = getline(&line, &len, file)) != -1) {
        if(read > 0) {
            amxc_string_set(&str, line);
            if(amxc_string_search(&str, "topic read testTopic2", 0) != -1) {
                foundTopic = true;
            }

            amxc_string_reset(&str);
        }
    }
    fclose(file);
    amxc_string_clean(&str);
    free(line);

    assert_true(foundTopic);
}

void test_can_start_plugin(UNUSED void** state) {
    assert_int_equal(_mqttbroker_main(0, mock_mqttbroker_get_dm(), &parser), 0);
}

void test_can_stop_plugin(UNUSED void** state) {
    assert_int_equal(_mqttbroker_main(1, mock_mqttbroker_get_dm(), &parser), 0);
}

void test_entry_point_succeeds_when_unhandled_reason_is_provided(UNUSED void** state) {
    assert_int_equal(_mqttbroker_main(99, mock_mqttbroker_get_dm(), &parser), 0);
}