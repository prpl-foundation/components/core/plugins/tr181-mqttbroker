TR181-MQTTBroker
============

Introduction and High-Level Description
---------------------------------------

An MQTT broker service will be available on the prplOS, exposed using the TR181 MQTT broker data model.

The MQTT broker is a micro-service using ambiorix framework and exposing it's HL API on the provided bus system.

The MQTT broker(s) can be used by MQTT clients on the device itself or by MQTT clients which are connected to the (LAN) network.

The service is highly configurable and relies on the opensource (lib)mosquitto implementation.

Use Case Definition
-------------------

Here are the main use cases:

*   It must be possible to install one or more brokers on the HGW.
    
    *   A broker configuration can be pre-configured or dynamically added and removed.
        
    *   All broker configurations must be applied independently from each other.
        
*   The broker configuration must be reboot and upgrade persistent.
    
*   A local MQTT Client must be able to connect to the (local) broker.
    
    *   The local broker must support connection from 'remote' (LAN) clients.
        
*   The broker instance will automatically open the firewall for the port(s) and the interface(s) it listens to.
    
*   A single broker instance can result in a single MQTT broker process, or if requested, one MQTT broker process can use different 'interface listeners'.
    
*   It must be possible to configure a list of user/password combinations for a broker configuration.
    
    *   Today Tr181 MQTT broker data model is limited to only one user, which is not sufficient for customer use cases.
        
*   The MQTT broker will allow two authentication possibilities:
    
    *   Username/Password authentication,
        
    *   Certificate based authentication.
        
*   The MQTT broker itself can be configured with a server certificate and corresponding private key.
    

References
----------

*   MQTT 5.0 specifications: [https://docs.oasis-open.org/mqtt/mqtt/v5.0/mqtt-v5.0.html](https://docs.oasis-open.org/mqtt/mqtt/v5.0/mqtt-v5.0.html)
    
*   Libmosquitto API documentation : [https://mosquitto.org/api/files/mosquitto-h.html](https://mosquitto.org/api/files/mosquitto-h.html)
    
*   TR-181 Datamodel: [TR-181 Device.MQTT.Broker.](https://usp-data-models.broadband-forum.org/tr-181-2-18-1-usp.html#D.Device:2.Device.MQTT.Broker.)
    

Architecture
============
![Broker Architecture diagram](image/MQTTBrokerArchitecture.png)

A broker can be used internally on prplOS or can be contacted from within the network, based on its configuration.

Implementation
==============

Low Level Architecture
----------------------
![Broker Low Level Architecture diagram](image/MQTTBrokerLowLevelArchitecture.png)

### MQTT Broker

This is the main service which exposes the MQTT broker functionality in the data model.

The amx plugin is a front-end for the open source MQTT broker application. (provided by libmosquitto [https://mosquitto.org/](https://mosquitto.org/))

A number of Vendor Extensions are added to fulfil customer requirements:

*   Support authentication using certificates
    
*   Multi-user support. Today this is not supported in the tr181 specification.
    
    *   A vendor extension data model is added to support a list of users: MQTTBroker.Broker.1.X\_PRPL-COM\_User.\[\]
        
    *   "mosquitto\_passwd" is used to configure username/password configurations for the mosquitto broker.
        
    *   These Users are not mapped (today) in Device
        
    *   .Users.
        

MQTT broker users are handled the 'mosquitto way'. A dedicated tool to generate a username password combination is used. These users are not in TR181 User management. (Device.Users.User.)

### Single process versus multiple broker processes.

It is possible to link a single data model instance with a single MQTT Broker process, but for some use cases, it must be possible to add different listener interfaces for a single broker process. This use case is not well supported by TR181 and therefore made possible by using a Vendor extension parameter.

*   MQTTBroker.Broker.2.X\_PRPL-COM\_BrokerID
    
*   If the brokerID is equal to another brokerID in the list, only one process will be used, if it is different, new process will be spawned.
    
*   This parameter is introduced to circumvent a limitation in TR181: (i.e. it is not possible to bind a MQTT broker process to multiple interfaces, multiple ports)
    

### Security of MQTT topics

For security purposes, it must be possible to run the broker as non-root process. This is done by reducing capabilities of the amxrt process.

*   If you want to run mosquitto broker as a non-root user, the user needs to be available in Device.Users.User (as a linux user)
    

The Mosquitto ACL file can be created based on the existence of instances **MQTTBroker.Broker.{i}.X\_PRPL-COM\_Users.{i}.ACL.{i}**.

When you set a topic and access rights for a user, it:

*   **read**: Allows the user to subscribe and read messages from the specified topic.
    
*   **write**: Allows the user to publish messages to the specified topic.
    
*   **readwrite**: Allows the user to both read from and publish to the specified topic.
    
*   **deny**: Explicitly denies any access (both read and write) to the specified topic for the user.
    

The ACL file is generated with all topics and access rights for all users configured within a listener or broker.

### MQTT Broker Status

The TR181-mqttbroker services updates the `Status` information parameter by parsing the output of the child MQTT broker process.

A possible improvement might be to add a (u)bus interface to the child MQTT broker process to query more precise status.

### mod\_dmext

mod\_dmext is used to implement some more advanced data model validators.

### mod\_pcm\_svc

mod\_pcm\_svc is used to ensure upgrade persistence. It will automatically register the MQTT broker with the PersistentConfiguration manager and provide export and import functionality.

All parameters which are marked with %upc or %usersetting will be taken into account for upgrade persistence.

### (lib)netmodel

NetModel is used to subscribe to Interface changes: I.e. the MQTT broker will only be started if the interface(s) on which the broker is active, is up.

### firewall (mod-fw-amx)

An MQTT broker needs a listening port (and interface) As the firewall is configured “closed” by default (INPUT policy is DROP), the MQTT broker instance must open the firewall to allow the broker to receive packets on the configured interface/port(s).

mod-fw-amx provides the Device.Firewall.setService() API to do so efficiently.

### MQTT Broker Stats

Broker statistics is currently unsupported by the plugin.

Datamodel
---

Datamodel is based on standard TR181 Data model API as defined in [BBF TR-181 datamodel](https://usp-data-models.broadband-forum.org/tr-181-2-18-1-usp.html#D.Device:2.Device.MQTT.Broker.) with  additional specific prpl vendor extensions.

Vendor extension documentaiton can be found in datamodel generated HL-API documentation on the [component gitlab wiki pages](https://tr181-mqttbroker-prpl-foundation-components-core-e964a364c94303.gitlab.io/datamodel/).

Files
-----

Basic .so files and odl definitions, defaults files are located under `/usr/lib/amx/tr181-mqttbroker/`

The saved configuration is stored under `/etc/config/tr181-mqttbroker/`

The Persistent configuration of MQTTBroker is stored in the file: `/cfg/pcm/tr181-mqttbroker_MQTTBroker.json`

Mosquitto installs a few (helper) tools and libraries

*   mosquitto, mosquitto\_passwd,  mosquitto\_rr, mosquitto\_ctrl, mosquitto\_pub, mosquitto\_sub
    
*   ./usr/lib/libmosquitto.so.1
    

The Mosquitto users are stored in a dedicated passwd file: `mosquitto.pass`

The start up script is installed here : `/etc/init.d/tr181-mqttbroker`

Firewall
--------

*   The MQTT broker service will open the firewall on the port(s) and the interface(s) it is configured to using the `SetService` API in the tr181 firewall plugin.
    

Dependants and Dependencies
---------------------------

### Dependants

*   Tr181-firewall
    
*   mod\_dmext
    
*   netmodel
    

### Dependencies

TR181-mqttbroker depends on the mosquitto broker implementation.

Configuration
-------------

By default, no MQTT broker instance is configured for prplOS.

Backup/Restore, Upgrade, Reset
------------------------------

All Parameters marked with %upc or %usersetting will be upgrade persistent.

All parameters marked with the %persistent flag will be reboot persistent.

Web UI
------

N/A

Considerations
==============

IPv6
----

The MQTT broker works for both IPv4 as on IPv6.

Packet Acceleration
-------------------

N/A

Memory Usage
------------

The Memory usage will be proportionally dependent on the number of MQTT broker instances(processes) started.

Boot Time
---------

The Boot time will be proportionally dependent on the number of MQTT broker instances(processes) started.

As they started in background and asynchronously, it should not have a big impact on overall startup time.

Security
--------

*   The MQTT broker can be started as a non-root user.
    
*   Certificate and username/password based authentication is supported.
    
*   `mosquitto.pass` contains password hashes.
    
*   MQTT’s ACL system is supported.
    
*   Mosquitto performs SO\_BINDTO\_DEVICE to the interface on which each listener is configured.
    
*   There is currently no way to configure on which IPv4 and IPv6 addresses it must listen; it listens on all IP addresses under the Device.IP. interface. However, firewall rules and interface binding protect from e.g. an IPv6 GUA listening on the LAN interface from being reached from a WAN interface.
    

Remote Management
-----------------

*   TR181 MQTT broker can be configured using the TR181 Data model.
    
*   This can be used by
    
    *   tr69
        
    *   tr369
        
    *   UI.
        

Operational Monitoring
----------------------

Syslog and /var/log/messages can be used to monitor the service.

Quality
=======

Unit Tests
----------

*   The component has unit tests coverage

Logging and Debugging
---------------------

*   A Trace library is used and can be configured to allow more debug logs to be activated.
    
