%define {
    /**
    * MQTTBroker Contains a list of configured MQTT brokers.
    * @version V1.0
    */
    %persistent object MQTTBroker {
        /**
        * @brief
        * Create and/or configure a broker.
        * If the 'id' exists, then the server is reconfigured.
        * Otherwise a new server is created and configured.
        * @param alias The broker identification.
        * @param broker The broker configuration
        * @return uint32_t 0 if the broker is added.
        * @Version V1.0
        */
        %protected bool addBroker(%in %mandatory string alias, %in %mandatory variant broker);

        /**
        * @brief
        * Delete a broker.
        * @param alias The broker identification.
        * @return uint32_t 0 if the broker is added.
        * @Version V1.0
        */
        %protected bool deleteBroker(%in %mandatory string alias);

        /**
        * @brief
        * Get broker configuration.
        * @param alias The broker identification.
        * @return broker variant containing the broker configuration.
        * @Version V1.0
        */
        %protected variant getBroker(%in %mandatory string alias);

        /**
        * @brief
        * Set the 'Enable' parameter of a certain broker.
        * @param alias The broker identification.
        * @param enable The value to set the 'Enable' parameter.
        * @return uint32_t 0 if the enable parameter is set.
        * @Version V1.0
        */
        %protected bool setEnable(%in %mandatory string alias, %in %mandatory bool enable = false);

        /**
        * If set to true, the log will include entries when clients connect and disconnect.
        * @Version V1.0
        */
        %persistent %protected bool ConnectionMessages = true;

        /**
        * User to which mosquitto will change if run as root. If run as a non-root user, this setting has no effect.
        * @version V1.0
        */
        %persistent %protected string RunAsUser {
            default "Device.Users.User.mosquitto-user.";
        }

        %persistent object Broker[] {
            counted with BrokerNumberOfEntries;
            on action destroy call mqtt_broker_cleanup;

            /**
            * A non-volatile unique key used to reference this instance.
            * @version V1.0
            */
            %persistent %unique %key string Alias {
                userflags %upc;
            }

            /**
            * The textual name of the MQTT broker
            * @version V1.0
            */
            %persistent string Name {
                userflags %upc;
                on action validate call check_maximum_length 64;
            }

            /**
            * The pid of the broker related process
            * @version V1.0
            */
            %protected %read-only uint32 PID;

            /**
            * Identifier of which mosquitto broker this instance will be added as a Listener.
            * If empty, it will be the single listener of a new broker.
            * @version V1.0
            */
            %protected %persistent uint32 '${prefix_}BrokerID' {
                userflags %upc;
                default 0;
            }

            /**
            * The path of the broker related config file
            * @version V1.0
            */
            %protected %read-only string ConfFile;

            /**
            * Specify an external module to use for authentication and access control.
            * @version V1.0
            */
            %persistent %protected string AuthPlugin;

            /**
            * Allows to enable or disable the MQTT broker.
            * @version V1.0
            */
            %persistent bool Enable = false;

            /**
            * Indicates the status of the MQTT broker.
            * @version V1.0
            */
            %read-only string Status {
                default "Disabled";
                on action validate call check_enum 
                    ["Disabled", "Enabled",
                    "Error_Misconfigured", "Error"];
            }

            /**
            * Port used by the MQTT Broker.
            * @version V1.0
            */
            %persistent uint32 Port {
                userflags %upc;
                default 1883;
            }

            /**
            * Indicates the Interface.
            * If an empty string is specified, the CPE MUST bind the MQTT broker to all available interface
            * @version V1.0
            */
            %persistent string Interface {
                userflags %upc;
                default "";
            }

            /**
            * Indicates the Interface.
            * IP Protocol Version
            * Available options are "ipv4", "ipv6" and "both".
            * @version V1.0
            */
            %persistent %protected string SocketDomain {
                userflags %upc;
                default "ipv4";
                on action validate call check_enum 
                    ["ipv4", "ipv6", "both"];
            }

            /**
            * Configures MQTT bridges, which are used to communicate with other MQTT brokers.
            * @version V1.0
            */
            %persistent object Bridge[] {
                counted with BridgeNumberOfEntries;

                /**
                * A non-volatile unique key used to reference this instance.
                * @version V1.0
                */
                %persistent %unique %key string Alias {
                    on action validate call check_maximum_length 64;
                }

                /**
                * The textual name of the MQTT bridge used for identification.
                * @version V1.0
                */
                %persistent string Name {
                    on action validate call check_maximum_length 64;
                }

                /**
                * Allows to enable or disable the MQTT Bridge.
                * @version V1.0
                */
                %persistent bool Enable = false;

                /**
                * Indicates the status of the MQTT bridge.
                * @version V1.0
                */
                %read-only string Status {
                    default "Disabled";
                    on action validate call check_enum 
                        ["Disabled", "Connecting", "Connected",
                        "Error_Misconfigured", "Error_BrokerUnreachable"];
                }

                /**
                * The MQTT client identifier used in the CONNECT message.
                * @version V1.0
                */
                %persistent string ClientID {
                    on action validate call check_maximum_length 256;
                }

                /**
                * Username used to authenticate the MQTT broker when making a connection over the MQTT bridge
                * @version V1.0
                */
                %persistent string Username {
                    on action validate call check_maximum_length 256;
                }

                /**
                * Password used to authenticate the MQTT broker when making a connection over the MQTT bridge 
                * @version V1.0
                */
                %persistent string Password {
                    on action validate call check_maximum_length 256;
                    on action read call hide_value;
                }

                /**
                * Specifies the MQTT protocol version used in MQTT bridge connection.
                * @version V1.0
                */
                %persistent string ProtocolVersion {
                    default "5";
                    on action validate call check_enum ["3.1", "3.1.1", "5"];
                }

                /**
                * If this flag is set to True, the remote MQTT broker will delete all subscription information after a Disconnect.
                * @version V1.0
                */
                %persistent bool CleanSession = true;

                /**
                * Keep Alive Time in seconds defines maximum wait time after which a message has to be sent to the remote MQTT broker
                * @version V1.0
                */
                %persistent uint32 KeepAliveTime {
                    default 60;
                }

                /**
                * The algorithm used to select a server entry from the Server table.
                * @version V1.0
                */
                %persistent string ServerSelectionAlgorithm {
                    default "Priority";
                    on action validate call check_enum ["Priority", "RoundRobin", "Random"];
                }

                /**
                * List of Servers representing a MQTT bridge server to be used for a Bridge. 
                * @version V1.0
                */
                %persistent object Server[] {
                    counted with ServerNumberOfEntries;

                    /**
                    * A non-volatile unique key used to reference this instance.
                    * @version V1.0
                    */
                    %persistent %unique %key string Alias {
                        on action validate call check_maximum_length 64;
                    }

                    /**
                    * Allows to enable or disable this server.
                    * @version V1.0
                    */
                    %persistent bool Enable = false;

                    /**
                    * The priority of this Server that is used by the Bridge when determining the Server to connect to.
                    * @version V1.0
                    */
                    %persistent uint32 Priority {
                        on action validate call check_range { min = 0, max = 65535};
                    }

                    /**
                    * This parameter specifies a relative weight for entries with the same Priority.
                    * @version V1.0
                    */
                    %persistent uint32 Weight {
                    }

                    /**
                    * The remote broker’s IP address or domain name.
                    * @version V1.0
                    */
                    %persistent string Address {
                        on action validate call check_maximum_length 256;
                    }

                    /**
                    * The port used to connect to the remote server.
                    * @version V1.0
                    */
                    %persistent uint32 Port {
                        on action validate call check_range { min = 1, max = 65535};
                        default 1883;
                    }
                }

                /**
                * List of MQTT subscriptions handled over the bridge.
                * @version V1.0
                */
                %persistent object Subscription[] {
                    counted with SubscriptionNumberOfEntries;

                    /**
                    * A non-volatile unique key used to reference this instance.
                    * @version V1.0
                    */
                    %persistent %unique %key string Alias {
                        on action validate call check_maximum_length 64;
                    }

                    /**
                    * Setting Enable to true activates the handling of this subscription instance.
                    * @version V1.0
                    */
                    %persistent bool Enable = false;

                    /**
                    * Indicates the status of the Subscription.
                    * @version V1.0
                    */
                    %read-only string Status {
                        default "Disabled";
                        on action validate call check_enum 
                            ["Enabled", "Disabled", "Error"];
                    }

                    /**
                    * The subscribed topic
                    * @version V1.0
                    */
                    %persistent string Topic {
                        on action validate call check_maximum_length 65535;
                    }

                    /**
                    * The direction of the subscription.
                    * @version V1.0
                    */
                    %persistent string Direction {
                        on action validate call check_enum ["out", "in", "both"];
                        default "out";
                    }

                    
                    /**
                    * The port used to connect to the remote server.
                    * @version V1.0
                    */
                    %persistent uint32 QoS {
                        on action validate call check_range { min = 0, max = 2};
                        default 0;
                    }

                    /**
                    * The local prefix is used for remapping received topics to the local topics of the MQTT broker
                    * and to select the topics to be sent over bridge to the remote MQTT broker.
                    * @version V1.0
                    */
                    %persistent string LocalPrefix;

                    /**
                    * The remote prefix is used for remapping topics to the remote MQTT broker topic lists.
                    * @version V1.0
                    */
                    %persistent string RemotePrefix;
                }
            }   

            /**
            * List of users allowed authenticate the MQTT clients, which connect to the MQTT broker.
            * @version V1.0
            */
            %persistent object '${prefix_}User'[] {
                counted with '${prefix_}UserNumberOfEntries';

                /**
                * A non-volatile unique key used to reference this instance.
                * @version V1.0
                */
                %persistent %unique %key string Alias {
                    userflags %upc;
                }

                /**
                * User name used to authenticate the MQTT clients, which connect to the MQTT broker.
                * If this parameter is an empty string no authentication is used.
                * @version V1.0
                */
                %persistent string Username {
                    userflags %upc;
                    on action validate call check_maximum_length 64;
                }

                /**
                * Password used used to authenticate the MQTT clients, which connect to the MQTT broker.
                * @version V1.0
                */
                %persistent string Password {
                    userflags %upc;
                    on action validate call check_maximum_length 256;
                    on action read call hide_value;
                }

                /**
                * List of topics access configuration
                * @version V1.0
                */
                %persistent object ACL[] {
                    counted with ACLNumberOfEntries;
                    
                    /**
                    * A non-volatile unique key used to reference this instance.
                    * @version V1.0
                    */
                    %persistent %unique %key string Alias {
                        userflags %upc;
                        on action validate call check_maximum_length 64;
                    }

                    /**
                    * MQTT Topic
                    * @version V1.0
                    */
                    %persistent string Topic {
                        userflags %upc;
                        on action validate call check_maximum_length 65535;
                    }

                    /**
                    * Configure access rights on specific topic
                    * @version V1.0
                    */
                    %persistent string AccessRights {
                        userflags %upc;
                        default "read";
                        on action validate call check_enum
                            ["read", "write", "readwrite", "deny"];
                    }
                }
            }

            /**
            * User name used to authenticate the MQTT clients, which connect to the MQTT broker.
            * Not Implemented
            * @version V1.0
            */
            %persistent string Username;

            /**
            * Password used used to authenticate the MQTT clients, which connect to the MQTT broker.
            * Not Implemented
            * @version V1.0
            */
            %persistent string Password;

            /**
            * RequireCertificate forces the client to provide a valid certificate otherwise the network connection will not proceed.
            * @Version V1.0
            */
            %persistent %protected bool RequireCertificate;

            
            /**
            * Use the CN value from the client certificate as a username.
            * @version V9.0
            */
            %persistent %protected bool UseIdentityAsUsername;


            /**
            * At least one of CAFile or CAPath must be provided to allow SSL support.
            * Define the path to a file containing the PEM encoded CA certificates that are trusted.
            * @Version V1.0
            */
            %persistent %protected string CAFile;

            /**
            * At least one of CAFile or CAPath must be provided to allow SSL support.
            * Define a directory that contains PEM encoded CA certificates that are trusted.
            * @Version V1.0
            */
            %persistent %protected string CAPath;

            /**
            * Path to the PEM encoded server certificate.
            * @Version V1.0
            */
            %persistent %protected string CertFile;

            /**
            * The list of allowed ciphers, each separated with a colon.
            * If empty, defaults to system ciphers (see DEFAULT_CIPHER_SUITE_LIST).
            * @Version V1.0
            */
            %persistent %protected string Ciphers;

            /**
            * If you have RequireCertificate set to true, you can create a certificate revocation list file to revoke access to particular client certificates.
            * @Version V1.0
            */
            %persistent %protected string CrlFile;

            /**
            * Path to the PEM encoded keyfile.
            * @Version V1.0
            */
            %persistent %protected string KeyFile;

            /**
            * Use TLS Engine to handle the certificates
            * @Version V1.0
            */
            %persistent %protected bool TlsEngine;
        }
    }    
}

%populate {
    on event "app:start" call mqtt_broker_start;

    on event "dm:object-changed" call mqtt_broker_toggled
    filter 'path matches "^MQTTBroker\.Broker\..*\.$" &&
            contains("parameters.Enable")';

    on event "dm:object-changed" call mqtt_broker_user_changed
    filter 'path matches "^MQTTBroker\.Broker\..*\.${prefix_}User\..*\.$"';

    on event "dm:object-changed" call mqtt_broker_bridge_server_changed
    filter 'path matches "^MQTTBroker\.Broker\..*\.Bridge\..*\.Server\..*\.$" &&
            !contains("parameters.Status")';

    on event "dm:object-changed" call mqtt_broker_bridge_sub_changed
    filter 'path matches "^MQTTBroker\.Broker\..*\.Bridge\..*\.Subscription\..*\.$" &&
            !contains("parameters.Status")';

    on event "dm:object-changed" call mqtt_broker_bridge_changed
    filter 'path matches "^MQTTBroker\.Broker\..*\.Bridge\..*\.$" &&
            !contains("parameters.Status")';

    on event "dm:instance-added" call mqtt_broker_auth_changed
    filter 'path matches "^MQTTBroker\.Broker\..*\.${prefix_}User\.$"';

    on event "dm:instance-removed" call mqtt_broker_auth_changed
    filter 'path matches "^MQTTBroker\.Broker\..*\.${prefix_}User\.$"';

    on event "dm:object-changed" call mqtt_broker_acl_changed
    filter 'path matches "^MQTTBroker\.Broker\..*\.${prefix_}User\..*\.ACL\..*\.$"';

    on event "dm:instance-added" call mqtt_broker_acl_changed
    filter 'path matches "^MQTTBroker\.Broker\..*\.${prefix_}User\..*\.ACL\.$"';

    on event "dm:instance-removed" call mqtt_broker_acl_changed
    filter 'path matches "^MQTTBroker\.Broker\..*\.${prefix_}User\..*\.ACL\.$"';

    on event "dm:object-changed" call mqtt_broker_updated
    filter 'path matches "^MQTTBroker\.Broker\..*\.$" &&
            !contains("parameters.Enable") &&
            !contains("parameters.PID") &&
            !contains("parameters.ConfFile") &&
            !contains("parameters.Status")';

    on event "dm:instance-added" call mqtt_broker_added
    filter 'path == "MQTT.Broker." &&
            parameters.Enable == true';

    on event "dm:instance-removed" call mqtt_broker_removed
    filter 'path == "MQTT.Broker."';
}
