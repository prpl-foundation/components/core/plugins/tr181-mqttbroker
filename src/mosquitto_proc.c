/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2023 SoftAtHome
**
** Redistribution and use in source and binary forms, with or without modification,
** are permitted provided that the following conditions are met:
**
** 1. Redistributions of source code must retain the above copyright notice,
** this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above copyright notice,
** this list of conditions and the following disclaimer in the documentation
** and/or other materials provided with the distribution.
**
** Subject to the terms and conditions of this license, each copyright holder
** and contributor hereby grants to those receiving rights under this license
** a perpetual, worldwide, non-exclusive, no-charge, royalty-free, irrevocable
** (except for failure to satisfy the conditions of this license) patent license
** to make, have made, use, offer to sell, sell, import, and otherwise transfer
** this software, where such license applies only to those patent claims, already
** acquired or hereafter acquired, licensable by such copyright holder or contributor
** that are necessarily infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright holders and
** non-copyrightable additions of contributors, in source or binary form) alone;
** or
**
** (b) combination of their Contribution(s) with the work of authorship to which
** such Contribution(s) was added by such copyright holder or contributor, if,
** at the time the Contribution is added, such addition causes such combination
** to be necessarily infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any copyright
** holder or contributor is granted under this license, whether expressly, by
** implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
** AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
** IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
** ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
** LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
** DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
** SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
** CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
** OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
** USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/
#include <stdlib.h>
#include <stdio.h>
#include <stdbool.h>
#include <string.h>
#include <pthread.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <errno.h>
#include <debug/sahtrace.h>
#include <debug/sahtrace_macros.h>
#include "mosquitto_proc.h"
#include "mqttbroker.h"
#include "dm_mqttbroker.h"
#include "firewall.h"
#include <netmodel/client.h>
#include <netmodel/common_api.h>

#define MOSQUITTO_CONF_PATH     MQTT_DIR
#define MOSQUITTO_CONF_FILE     "mosquitto.conf"
#define MOSQUITTO_PASSWORD_FILE "mosquitto.pass"
#define MOSQUITTO_ACL_FILE "mosquitto.acl"

#define PROCESS "process_mqttbroker"

#ifndef CONFIG_DEFAULT_CIPHER_SUITE_LIST
#define CONFIG_DEFAULT_CIPHER_SUITE_LIST ""
#endif

#define string_empty(X) ((X == NULL) || (*X == '\0'))
#define BUFFER_SIZE 1024

typedef struct {
    char address[256];
    uint32_t port;
    uint32_t priority;
    uint32_t weight;
} server_class_t;
typedef struct process_t {
    pid_t pid;
    int fd_stdout;
    int fd_stderr;
    amxc_llist_it_t it;
} process_t;

amxp_slot_fn_t termination_callback = NULL;
amxc_llist_t* process_list = NULL;

void process_cleanup(void) {
    amxc_llist_delete(&process_list, NULL);
}

static process_t* create_broker_process(pid_t pid, int fd_stdout, int fd_stderr) {
    process_t* new_process = NULL;

    if(process_list == NULL) {
        amxc_llist_new(&process_list);
        when_null(process_list, exit);
    }

    new_process = (process_t*) calloc(1, sizeof(process_t));
    when_null_trace(new_process, exit, ERROR, "Could not allocate memory");

    new_process->pid = pid;
    new_process->fd_stdout = fd_stdout;
    new_process->fd_stderr = fd_stderr;

    if(amxc_llist_append(process_list, &new_process->it) != 0) {
        SAH_TRACEZ_ERROR(ME, "Failed to add process to list");
        free(new_process);
        new_process = NULL;
    }

exit:
    return new_process;
}

static process_t* find_broker_process_by_pid(pid_t pid) {
    process_t* process = NULL;
    amxc_llist_for_each(it, process_list) {
        process = amxc_llist_it_get_data(it, process_t, it);
        if(process->pid == pid) {
            break;
        }
    }
    return process;
}

static int delete_broker_process_by_pid(pid_t pid) {
    int ret = -1;
    process_t* process = find_broker_process_by_pid(pid);
    if((process == NULL)) {
        SAH_TRACEZ_ERROR(ME, "Process with PID %d not found", pid);
        goto exit;
    }

    if((process->pid != pid)) {
        SAH_TRACEZ_ERROR(ME, "Process with PID %d not found", pid);
        goto exit;
    }

    amxc_llist_it_take(&process->it);
    free(process);

    if(amxc_llist_is_empty(process_list)) {
        amxc_llist_delete(&process_list, NULL);
        process_list = NULL;
    }

    ret = 0;
exit:
    return ret;
}

static int get_fd_by_pid(pid_t pid, const char* fd_type) {
    process_t* process = find_broker_process_by_pid(pid);
    int ret = 0;
    when_null(process, exit);
    if(string_empty(fd_type)) {
        goto exit;
    }

    if(strcmp(fd_type, "stdout") == 0) {
        ret = process->fd_stdout;
    }
    if(strcmp(fd_type, "stderr") == 0) {
        ret = process->fd_stderr;
    }
exit:
    return ret;
}

static inline char* broker_process_get_file_path(const char* alias, const char* file) {
    amxc_string_t path;
    char* ret = NULL;

    when_null(alias, exit);
    when_null(file, exit);

    amxc_string_init(&path, 0);
    amxc_string_setf(&path, "%s%s_%s", MOSQUITTO_CONF_PATH, alias, file);
    ret = amxc_string_take_buffer(&path);
    amxc_string_clean(&path);
exit:
    return ret;
}

static bool broker_process_create_empty_file(const char* path) {
    FILE* fPointer = fopen(path, "w");
    if(fPointer == NULL) {
        SAH_TRACEZ_ERROR(ME, "Failed to create MQTTBroker file: %s", path);
        return false;
    }
    fclose(fPointer);
    return true;
}

static bool broker_process_deleteConfig(char* path) {
    bool ret = true;
    if(access(path, F_OK) != 0) {
        // File does not exist
        return true;
    }

    if(unlink(path) == -1) {
        SAH_TRACEZ_NOTICE(ME, "Failed to unlink mosquitto file: %s", path);
        ret = false;
    }
    return ret;
}

static void broker_clean_files(amxd_object_t* object) {
    char* alias = amxd_object_get_value(cstring_t, object, "Alias", NULL);
    char* confFile = amxd_object_get_value(cstring_t, object, "ConfFile", NULL);
    char* passwdPath = broker_process_get_file_path(alias, MOSQUITTO_PASSWORD_FILE);
    char* aclPath = broker_process_get_file_path(alias, MOSQUITTO_ACL_FILE);

    broker_process_deleteConfig(passwdPath);
    broker_process_deleteConfig(aclPath);
    broker_process_deleteConfig(confFile);

    update_param_uint32(object, "PID", 0);
    update_param_string(object, "ConfFile", "");

    free(alias);
    free(passwdPath);
    free(aclPath);
    free(confFile);
}

static bool broker_process_create_passwd_file(amxd_object_t* object, const char* path) {
    bool res = false;
    struct stat buffer;
    amxc_string_t templ_str;
    cstring_t username = NULL;
    cstring_t password = NULL;
    amxp_subproc_t* process_passwd = NULL;
    amxd_object_t* users = NULL;

    when_null(object, exit);

    amxc_string_init(&templ_str, 0);
    amxc_string_setf(&templ_str, "%sUser", get_vendor_prefix());
    users = amxd_object_get_child(object, amxc_string_get(&templ_str, 0));
    amxc_string_clean(&templ_str);

    int exist = stat(path, &buffer);

    if(exist != 0) {
        broker_process_create_empty_file(path);
    }

    amxd_object_for_each(instance, it, users) {
        amxd_object_t* instance = amxc_container_of(it, amxd_object_t, it);
        username = amxd_object_get_value(cstring_t, instance, "Username", NULL);
        password = amxd_object_get_value(cstring_t, instance, "Password", NULL);
        bool ok_process = false;

        if((string_empty(username)) || (string_empty(password))) {
            free(username);
            free(password);
            continue;
        }

        if(amxp_subproc_new(&process_passwd) != 0) {
            SAH_TRACEZ_ERROR(ME, "Failed to create mosquitto_passwd process");
            goto err;
        }

        ok_process = amxp_subproc_start_wait(process_passwd, 1000, (char*) "mosquitto_passwd", "-b", path, username, password, NULL); // add user and password to password file

        if(ok_process) {
            SAH_TRACEZ_ERROR(ME, "Failed to run mosquitto_passwd");
        }
        amxp_subproc_delete(&process_passwd);
err:
        free(username);
        free(password);
    }
    res = true;

exit:
    return res;
}

static bool broker_add_to_acl_file(amxd_object_t* userObj, const char* path, const char* user) {
    FILE* file = NULL;
    char line[BUFFER_SIZE] = {0};
    int user_found = 0;
    bool res = false;
    cstring_t topic = NULL;
    cstring_t access = NULL;
    amxd_object_t* acl = NULL;
    amxc_string_t user_prefix;

    when_null(userObj, exit);
    when_null(path, exit);
    when_null(user, exit);

    acl = amxd_object_get_child(userObj, "ACL");

    file = fopen(path, "a+");
    if(!file) {
        SAH_TRACEZ_ERROR(ME, "Error opening file");
        return res;
    }

    amxc_string_init(&user_prefix, 0);
    amxc_string_setf(&user_prefix, "user %s\n", user);

    while(fgets(line, sizeof(line), file)) {
        if(strcmp(line, amxc_string_get(&user_prefix, 0)) == 0) {
            user_found = 1;
            break;
        }
    }

    if(!user_found) {
        fprintf(file, "\nuser %s\n", user);
    }

    amxd_object_for_each(instance, it, acl) {
        amxd_object_t* instance = amxc_container_of(it, amxd_object_t, it);

        topic = amxd_object_get_value(cstring_t, instance, "Topic", NULL);
        access = amxd_object_get_value(cstring_t, instance, "AccessRights", NULL);

        if((string_empty(topic)) || (string_empty(access))) {
            free(topic);
            free(access);
            continue;
        }

        fprintf(file, "topic %s %s\n", access, topic);

        free(access);
        free(topic);
    }

    fclose(file);
    amxc_string_clean(&user_prefix);
    res = true;
exit:
    return res;
}

static bool broker_process_manage_acl(amxd_object_t* object, const char* path) {
    bool res = false;
    struct stat buffer;
    amxc_string_t templ_str;
    cstring_t username = NULL;
    amxd_object_t* users = NULL;
    int aclEntriesUser = 0;
    int aclEntriesTotal = 0;

    when_null(object, exit);

    amxc_string_init(&templ_str, 0);
    amxc_string_setf(&templ_str, "%sUser", get_vendor_prefix());
    users = amxd_object_get_child(object, amxc_string_get(&templ_str, 0));
    amxc_string_clean(&templ_str);

    int exist = stat(path, &buffer);

    if(exist != 0) {
        broker_process_create_empty_file(path);
    }

    amxd_object_for_each(instance, it, users) {
        amxd_object_t* instance = amxc_container_of(it, amxd_object_t, it);
        username = amxd_object_get_value(cstring_t, instance, "Username", NULL);

        if(string_empty(username)) {
            free(username);
            continue;
        }

        aclEntriesUser += amxd_object_get_value(uint32_t, instance, "ACLNumberOfEntries", NULL);
        if(aclEntriesUser > 0) {
            aclEntriesTotal += aclEntriesUser;
            broker_add_to_acl_file(instance, path, username);
        }

        free(username);
    }

    if(aclEntriesTotal > 0) {
        res = true;
    }

exit:
    return res;
}

static char* broker_process_query_user(const char* userPath) {
    amxb_bus_ctx_t* bus_ctx = amxb_be_who_has("Users");
    amxc_string_t templ_str;
    amxc_var_t res;
    const char* tmp = NULL;
    char* ret = NULL;

    amxc_string_init(&templ_str, 0);
    amxc_var_init(&res);

    if(string_empty(userPath)) {
        goto exit;
    }

    when_null_trace(bus_ctx, exit, ERROR, "No bus_ctx");

    amxc_string_setf(&templ_str, "%s.", userPath);
    amxc_string_replace(&templ_str, "..", ".", UINT32_MAX);
    amxc_string_replace(&templ_str, "Device.", "", UINT32_MAX);

    if(amxb_get(bus_ctx, amxc_string_get(&templ_str, 0), 0, &res, 10) != AMXB_STATUS_OK) {
        SAH_TRACEZ_ERROR(ME, "Failed to get user '%s' username", amxc_string_get(&templ_str, 0));
    }

    tmp = GETP_CHAR(&res, "0.0.Username");

    if(!tmp) {
        goto exit;
    }

    ret = strdup(tmp);
exit:
    amxc_string_clean(&templ_str);
    amxc_var_clean(&res);
    return ret;
}

static void slot_proc_stop(UNUSED const char* const sig_name,
                           UNUSED const amxc_var_t* const data,
                           UNUSED void* priv) {
    uint32_t brokerPid = 0;
    uint32_t pid = amxc_var_dyncast(uint32_t, amxc_var_get_key(data, "PID", AMXC_VAR_FLAG_DEFAULT));
    amxd_object_t* brokers = amxd_dm_findf(mqttbroker_get_dm(), "MQTTBroker.Broker.");

    amxd_object_for_each(instance, it, brokers) {
        amxd_object_t* instance = amxc_container_of(it, amxd_object_t, it);
        brokerPid = amxd_object_get_value(uint32_t, instance, "PID", NULL);
        if(brokerPid == pid) {
            SAH_TRACEZ_WARNING(ME, "AMX Signal recieved : %s => process id = %d (changing broker status to ERROR)", sig_name, pid);
            broker_clean_files(instance);
            update_param_string(instance, "Status", "Error");
            close_firewall(instance);
        }
    }
}

static bool check_if_conf_file_exists(amxd_object_t* object) {
    struct stat buffer;
    char* confFile = amxd_object_get_value(cstring_t, object, "ConfFile", NULL);
    int exist = stat(confFile, &buffer);
    free(confFile);
    if(exist == 0) {
        return true;
    } else {
        return false;
    }
}

static bool broker_process_unsetFW(amxd_object_t* object) {
    bool res = false;
    amxd_object_t* brokers = amxd_dm_findf(mqttbroker_get_dm(), "MQTTBroker.Broker.");

    when_null(object, exit);
    when_null(brokers, exit);

    amxd_object_for_each(instance, it, brokers) {
        amxd_object_t* instance = amxc_container_of(it, amxd_object_t, it);
        if((is_listener(object, instance))) {
            close_firewall(instance);
        }
    }
    res = true;
exit:
    return res;
}

static bool broker_process_setFW(amxd_object_t* object) {
    bool res = true;
    uint32_t enable = 0;
    cstring_t status = NULL;
    amxd_object_t* brokers = amxd_dm_findf(mqttbroker_get_dm(), "MQTTBroker.Broker.");

    when_null(object, exit);
    when_null(brokers, exit);

    status = amxd_object_get_value(cstring_t, object, "Status", NULL);
    if(strcmp(status, "Enabled") == 0) {
        res = true;
        goto exit;
    }

    amxd_object_for_each(instance, it, brokers) {
        amxd_object_t* instance = amxc_container_of(it, amxd_object_t, it);
        enable = amxd_object_get_value(uint32_t, instance, "Enable", NULL);
        if((enable) && (is_listener(object, instance))) {
            if(!open_firewall(instance)) {
                broker_process_unsetFW(object); //unset firewall of the other listeners of the failed broker
                res = false;
                goto exit;
            }
        }
    }

    res = true;

exit:
    free(status);
    return res;
}

static int compareServersByPriorityAndWeight(amxc_array_it_t* a, amxc_array_it_t* b) {
    const server_class_t* serverA = (const server_class_t*) a->data;
    const server_class_t* serverB = (const server_class_t*) b->data;

    if(serverA->priority < serverB->priority) {
        return -1;
    } else if(serverA->priority > serverB->priority) {
        return 1;
    }

    if(serverA->weight < serverB->weight) {
        return -1;
    } else if(serverA->weight > serverB->weight) {
        return 1;
    }

    return 0;
}

static int compareServersRandomly(UNUSED amxc_array_it_t* a, UNUSED amxc_array_it_t* b) {
    return (rand() % 3) - 1;
}
static void sortServers(amxc_array_t* serversArray, int usePriority) {
    when_null(serversArray, exit);

    if(amxc_array_is_empty(serversArray)) {
        goto exit;
    }

    if(usePriority) {
        amxc_array_sort(serversArray, compareServersByPriorityAndWeight);
    } else {
        amxc_array_sort(serversArray, compareServersRandomly);
    }
exit:
    return;
}

static void addServer(amxc_array_t* serversArray, const char* address, uint32_t port, uint32_t priority, uint32_t weight) {
    server_class_t* server = (server_class_t*) calloc(1, sizeof(server_class_t));
    when_null(server, exit);

    strncpy(server->address, address, sizeof(server->address) - 1);
    server->address[sizeof(server->address) - 1] = '\0';

    server->port = port;
    server->priority = priority;
    server->weight = weight;
    amxc_array_grow(serversArray, 1);
    amxc_array_append_data(serversArray, (void*) server);
exit:
    return;
}

static bool buildServerString(amxc_array_t* servers_array, amxc_string_t servers_str, FILE* fPointer) {
    int32_t size = 0;
    int count = 0;
    bool ret = false;

    if((servers_array == NULL) || (amxc_array_is_empty(servers_array))) {
        goto exit;
    }

    size = amxc_array_size(servers_array);

    for(int i = 0; i < size; i++) {
        server_class_t* server = (server_class_t*) amxc_array_get_data_at(servers_array, i);
        if(server) {
            if(count > 0) {
                amxc_string_appendf(&servers_str, ", ");
            }
            amxc_string_appendf(&servers_str, "%s:%d", server->address, server->port);
            count++;
        }
    }

    if(count == 0) {
        goto exit;
    } else if(count > 1) {
        fprintf(fPointer, "addresses %s\n", amxc_string_get(&servers_str, 0));
    } else {
        fprintf(fPointer, "address %s\n", amxc_string_get(&servers_str, 0));
    }
    ret = true;
exit:
    return ret;
}

static void broker_array_it_free(amxc_array_it_t* it) {
    free(it->data);
}

static bool broker_process_set_bridge_servers(FILE* fPointer, amxd_object_t* object) {
    bool res = false;
    amxd_object_t* servers_obj = NULL;
    amxc_array_t servers_array;
    amxc_string_t servers_str;

    amxc_array_init(&servers_array, 0);
    amxc_string_init(&servers_str, 0);

    when_null(object, exit);
    servers_obj = amxd_object_get_child(object, "Server");

    amxd_object_for_each(instance, it, servers_obj) {
        amxd_object_t* instance = amxc_container_of(it, amxd_object_t, it);
        uint32_t enable = amxd_object_get_value(uint32_t, instance, "Enable", NULL);
        cstring_t address = amxd_object_get_value(cstring_t, instance, "Address", NULL);
        uint32_t port = amxd_object_get_value(uint32_t, instance, "Port", NULL);
        uint32_t priority = amxd_object_get_value(uint32_t, instance, "Priority", NULL);
        uint32_t weight = amxd_object_get_value(uint32_t, instance, "Weight", NULL);

        if((enable) && (!string_empty(address))) {
            addServer(&servers_array, address, port, priority, weight);
        }

        free(address);
    }
    sortServers(&servers_array, 1);
    res = buildServerString(&servers_array, servers_str, fPointer);
exit:
    amxc_array_clean(&servers_array, broker_array_it_free);
    amxc_string_clean(&servers_str);
    return res;
}

static bool broker_process_set_bridge_subs(FILE* fPointer, amxd_object_t* object) {
    bool res = false;
    amxd_object_t* subs = NULL;


    when_null(object, exit);
    subs = amxd_object_get_child(object, "Subscription");

    amxd_object_for_each(instance, it, subs) {
        amxd_object_t* instance = amxc_container_of(it, amxd_object_t, it);
        uint32_t enable = amxd_object_get_value(uint32_t, instance, "Enable", NULL);
        cstring_t alias = amxd_object_get_value(cstring_t, instance, "Alias", NULL);
        cstring_t topic = amxd_object_get_value(cstring_t, instance, "Topic", NULL);
        cstring_t localPrefix = amxd_object_get_value(cstring_t, instance, "LocalPrefix", NULL);
        cstring_t remotePrefix = amxd_object_get_value(cstring_t, instance, "RemotePrefix", NULL);
        cstring_t direction = amxd_object_get_value(cstring_t, instance, "Direction", NULL);
        uint32_t qos = amxd_object_get_value(uint32_t, instance, "QoS", NULL);
        amxc_string_t topic_str;
        amxc_string_t local_pre_str;
        amxc_string_t remote_pre_str;

        amxc_string_init(&topic_str, 0);
        amxc_string_init(&local_pre_str, 0);
        amxc_string_init(&remote_pre_str, 0);

        amxc_string_setf(&topic_str, "%s", topic);
        amxc_string_setf(&local_pre_str, "%s", localPrefix);
        amxc_string_setf(&remote_pre_str, "%s", remotePrefix);

        if(string_empty(topic)) {
            amxc_string_setf(&topic_str, "\"\"");
            if(string_empty(localPrefix) || string_empty(remotePrefix)) {
                SAH_TRACEZ_ERROR(ME, "Topic %s pattern is not valid", alias);
                update_param_string(instance, "Status", "Error");
                enable = 0;
            }
        } else {
            if(string_empty(localPrefix)) {
                amxc_string_setf(&local_pre_str, "\"\"");
            }
            if(string_empty(remotePrefix)) {
                amxc_string_setf(&remote_pre_str, "\"\"");
            }
        }

        if(enable) {
            fprintf(fPointer, "topic %s %s %d %s %s\n", amxc_string_get(&topic_str, 0), direction, qos, amxc_string_get(&local_pre_str, 0), amxc_string_get(&remote_pre_str, 0));
            update_param_string(instance, "Status", "Enabled");
        }

        free(alias);
        free(topic);
        free(localPrefix);
        free(remotePrefix);
        free(direction);
        amxc_string_clean(&topic_str);
        amxc_string_clean(&local_pre_str);
        amxc_string_clean(&remote_pre_str);
    }

    res = true;
exit:
    return res;
}

static bool broker_process_set_bridges(FILE* fPointer, amxd_object_t* object) {
    bool res = false;
    amxd_object_t* bridges = NULL;

    when_null(object, exit);

    bridges = amxd_object_get_child(object, "Bridge");
    amxd_object_for_each(instance, it, bridges) {
        amxd_object_t* instance = amxc_container_of(it, amxd_object_t, it);
        uint32_t enable = amxd_object_get_value(uint32_t, instance, "Enable", NULL);
        cstring_t alias = amxd_object_get_value(cstring_t, instance, "Alias", NULL);
        cstring_t name = amxd_object_get_value(cstring_t, instance, "Name", NULL);
        cstring_t clientId = amxd_object_get_value(cstring_t, instance, "ClientID", NULL);
        cstring_t username = amxd_object_get_value(cstring_t, instance, "Username", NULL);
        cstring_t password = amxd_object_get_value(cstring_t, instance, "Password", NULL);
        cstring_t protocolVersion = amxd_object_get_value(cstring_t, instance, "ProtocolVersion", NULL);
        cstring_t serverSelectionAlgorithm = amxd_object_get_value(cstring_t, instance, "ServerSelectionAlgorithm", NULL);
        bool cleanSession = amxd_object_get_value(bool, instance, "CleanSession", NULL);
        uint32_t keepAliveTime = amxd_object_get_value(uint32_t, instance, "KeepAliveTime", NULL);
        uint32_t serverEntries = amxd_object_get_value(uint32_t, instance, "ServerNumberOfEntries", NULL);

        if(enable) {
            if(!string_empty(name)) {
                fprintf(fPointer, "connection %s\n", name);
            } else {
                fprintf(fPointer, "connection %s\n", alias);
            }
            if(!string_empty(clientId)) {
                fprintf(fPointer, "remote_clientid %s\n", clientId);
            }
            if((!string_empty(username)) && (!string_empty(password))) {
                fprintf(fPointer, "remote_username %s\n", username);
                fprintf(fPointer, "remote_password %s\n", password);
            }
            if(!string_empty(protocolVersion)) {
                if(strcmp(protocolVersion, "3.1") == 0) {
                    fprintf(fPointer, "bridge_protocol_version mqttv31\n");
                }
                if(strcmp(protocolVersion, "3.1.1") == 0) {
                    fprintf(fPointer, "bridge_protocol_version mqttv311\n");
                }
                if(strcmp(protocolVersion, "5") == 0) {
                    fprintf(fPointer, "bridge_protocol_version mqttv50\n");
                }
            }
            if((serverEntries > 1) && (!string_empty(serverSelectionAlgorithm)) && (strcmp(serverSelectionAlgorithm, "RoundRobin") == 0)) {
                fprintf(fPointer, "round_robin true\n");
            }
            fprintf(fPointer, "cleansession %s\n", cleanSession ? "true" : "false");
            fprintf(fPointer, "keepalive_interval %d\n", keepAliveTime);
            broker_process_set_bridge_servers(fPointer, instance);
            broker_process_set_bridge_subs(fPointer, instance);
            fprintf(fPointer, "\n");
        }

        free(name);
        free(alias);
        free(clientId);
        free(username);
        free(password);
        free(protocolVersion);
        free(serverSelectionAlgorithm);
    }
    res = true;
exit:
    return res;
}

static bool broker_process_writeConfig(amxd_object_t* object) {
    bool ret = false;
    amxd_object_t* mqttbroker = amxd_dm_findf(mqttbroker_get_dm(), "MQTTBroker.");
    amxd_object_t* brokers = amxd_dm_findf(mqttbroker_get_dm(), "MQTTBroker.Broker.");
    char* runAsUserPath = amxd_object_get_value(cstring_t, mqttbroker, "RunAsUser", NULL);
    bool connectionMessage = amxd_object_get_value(bool, mqttbroker, "ConnectionMessages", NULL);
    char* brokerAlias = amxd_object_get_value(cstring_t, object, "Alias", NULL);
    char* runAsUser = broker_process_query_user(runAsUserPath);
    uint32_t brokerID = get_BrokerID(object);
    char* confPath = NULL;
    amxc_string_t str;
    FILE* fPointer = NULL;

    amxc_string_init(&str, 0);
    amxc_string_setf(&str, "broker_%d", brokerID);
    confPath = broker_process_get_file_path(amxc_string_get(&str, 0), MOSQUITTO_CONF_FILE);
    amxc_string_clean(&str);

    fPointer = fopen(confPath, "w");

    if(fPointer == NULL) {
        // exit if not succeeded
        SAH_TRACEZ_ERROR(ME, "Failed to create MQTTBroker config file");
        goto exit;
    }

    fprintf(fPointer, "# Automatically generated configuration file\n");
    fprintf(fPointer, "# Do not change directly, use the MQTTBroker plug-in\n");
    fprintf(fPointer, "\n");
    fprintf(fPointer, "# General\n");

    if(!string_empty(runAsUser)) {
        fprintf(fPointer, "user %s\n", runAsUser);
    } else {
        fprintf(fPointer, "user root\n");
    }

    fprintf(fPointer, "log_type all\n");
    fprintf(fPointer, "log_dest stdout\n");
    fprintf(fPointer, "connection_messages %s\n", connectionMessage ? "true" : "false");

    fprintf(fPointer, "per_listener_settings true\n");
    fprintf(fPointer, "\n");

    amxd_object_for_each(instance, it, brokers) {
        amxd_object_t* instance = amxc_container_of(it, amxd_object_t, it);
        char* interface = amxd_object_get_value(cstring_t, instance, "Interface", NULL);
        amxc_var_t* netmodel_result = netmodel_getFirstParameter(interface, "NetDevName", NULL, NULL);
        char* newInterface = amxc_var_dyncast(cstring_t, netmodel_result);
        char* socketDomain = amxd_object_get_value(cstring_t, instance, "SocketDomain", NULL);
        uint32_t enable = amxd_object_get_value(uint32_t, instance, "Enable", NULL);
        char* alias = amxd_object_get_value(cstring_t, instance, "Alias", NULL);
        char* plugin = amxd_object_get_value(cstring_t, instance, "AuthPlugin", NULL);
        uint32_t port = amxd_object_get_value(uint32_t, instance, "Port", NULL);
        char* certFile = amxd_object_get_value(cstring_t, instance, "CertFile", NULL);
        char* keyFile = amxd_object_get_value(cstring_t, instance, "KeyFile", NULL);
        char* ciphers = amxd_object_get_value(cstring_t, instance, "Ciphers", NULL);
        bool engine = amxd_object_get_value(bool, instance, "TlsEngine", NULL);
        bool requireCert = amxd_object_get_value(bool, instance, "RequireCertificate", NULL);
        char* caFile = amxd_object_get_value(cstring_t, instance, "CAFile", NULL);
        char* caPath = amxd_object_get_value(cstring_t, instance, "CAPath", NULL);
        char* crlFile = amxd_object_get_value(cstring_t, instance, "CrlFile", NULL);
        bool idAsUsername = amxd_object_get_value(bool, instance, "UseIdentityAsUsername", NULL);
        char* passwdPath = broker_process_get_file_path(alias, MOSQUITTO_PASSWORD_FILE);
        char* aclPath = broker_process_get_file_path(alias, MOSQUITTO_ACL_FILE);
        uint32_t usersEntries = 0;
        amxc_string_t templ_str;

        amxc_string_init(&templ_str, 0);
        amxc_string_setf(&templ_str, "%sUserNumberOfEntries", get_vendor_prefix());
        usersEntries = amxd_object_get_value(uint32_t, instance, amxc_string_get(&templ_str, 0), NULL);
        amxc_string_clean(&templ_str);

        if((enable) && (is_listener(object, instance))) {
            update_param_string(object, "ConfFile", confPath);

            fprintf(fPointer, "# Listener '%s'\n", alias);
            fprintf(fPointer, "\n");
            fprintf(fPointer, "listener %d\n", port);
            fprintf(fPointer, "bind_interface %s\n", newInterface);

            if(!string_empty(socketDomain)) {
                if((strcmp(socketDomain, "ipv4") == 0) || (strcmp(socketDomain, "ipv6") == 0)) {
                    fprintf(fPointer, "socket_domain %s\n", socketDomain);
                }
            }

            if(usersEntries > 0) {
                fprintf(fPointer, "allow_anonymous false\n");
                fprintf(fPointer, "password_file %s\n", passwdPath);
                if(!broker_process_create_passwd_file(instance, passwdPath)) {
                    SAH_TRACEZ_WARNING(ME, "Failed to create password file");
                }
                if(broker_process_manage_acl(instance, aclPath)) {
                    fprintf(fPointer, "acl_file %s\n", aclPath);
                }
            } else {
                if(!idAsUsername) {
                    fprintf(fPointer, "allow_anonymous true\n");
                }
            }

            if((!string_empty(certFile)) && (!string_empty(keyFile))) {
                fprintf(fPointer, "certfile %s\n", certFile);
                fprintf(fPointer, "keyfile %s\n", keyFile);
                if(!string_empty(ciphers)) {
                    // Check upon keyFile just to determine whether we want to use TLS or not.
                    fprintf(fPointer, "ciphers %s\n", ciphers);
                } else {
                    fprintf(fPointer, "ciphers %s\n", CONFIG_DEFAULT_CIPHER_SUITE_LIST);
                }
                if(engine) {
                    const char* tls_engine = getenv("DEFAULT_SSL_ENGINE_ID");
                    if(tls_engine && (tls_engine[0] != '\0')) {
                        fprintf(fPointer, "tls_engine %s\n", tls_engine);
                    } else {
                        fprintf(fPointer, "tls_engine ssle-client\n");
                    }
                    fprintf(fPointer, "tls_keyform engine\n");
                }
            }

            if(requireCert) {
                fprintf(fPointer, "require_certificate true\n");
                if(!string_empty(caFile)) {
                    fprintf(fPointer, "cafile %s\n", caFile);
                }
                if(!string_empty(caPath)) {
                    fprintf(fPointer, "capath %s\n", caPath);
                }
                if(!string_empty(crlFile)) {
                    fprintf(fPointer, "crlfile %s\n", crlFile);
                }
                if(idAsUsername) {
                    fprintf(fPointer, "use_identity_as_username true\n");
                }
            }

            if(!string_empty(plugin)) {
                fprintf(fPointer, "plugin %s\n", plugin);
            }

            fprintf(fPointer, "\n");
            broker_process_set_bridges(fPointer, instance);
            fprintf(fPointer, "\n");
        }

        free(socketDomain);
        free(plugin);
        free(alias);
        free(interface);
        free(newInterface);
        free(certFile);
        free(keyFile);
        free(ciphers);
        free(caFile);
        free(caPath);
        free(crlFile);
        free(passwdPath);
        free(aclPath);
    }

    fclose(fPointer);
    ret = true;

exit:
    free(confPath);
    free(brokerAlias);
    free(runAsUserPath);
    free(runAsUser);

    SAH_TRACEZ_NOTICE(ME, "MQTTBroker Config File Created");
    return ret;
}

/*
 * Returns:
 *  1 : path is directory
 *  0 : path does not exist
 * -1 : path exists but is no directory
 */
static int broker_process_isDir(const char* path) {
    struct stat statbuf;

    if(!stat(path, &statbuf)) {
        if(S_ISDIR(statbuf.st_mode)) {
            return 1;
        }

        SAH_TRACEZ_ERROR(ME, "Path '%s' exists but is no directory", path);
        return -1;
    }

    return 0;
}

static bool broker_process_checkDir(const char* path) {
    bool retval = false;
    char* mypath = NULL, * part = NULL, * p = NULL;

    if(!path) {
        SAH_TRACEZ_ERROR(ME, "Invalid argument");
        return false;
    }

    /* Shortcut check */
    switch(broker_process_isDir(path)) {
    case 1:
        return true;
        break;
    case -1:
        return false;
        break;
    default:
        break;
    }

    mypath = strdup(path);
    part = mypath;
    if(mypath[0] == '/') {
        part++;
    }

    while(true) {
        p = strchr(part, '/');
        if(p) {
            *p = 0;
        }

        switch(broker_process_isDir(mypath)) {
        case -1:
            goto error;
            break;
        case 0:
            if(mkdir(mypath, S_IRWXU | S_IRGRP | S_IXGRP | S_IROTH | S_IXOTH)) {
                SAH_TRACEZ_ERROR(ME, "Failed to create directory '%s': %m", mypath);
                goto error;
            }
            break;
        default:
            break;
        }

        if(!p) {
            break;
        }
        *p = '/';

        part = p + 1;
    }

    retval = true;

error:
    free(mypath);
    return retval;
}

static bool broker_process_disable_subscriptions(amxd_object_t* bridge_obj) {
    bool res = false;
    when_null(bridge_obj, exit);
    amxd_object_t* sub_obj = amxd_object_get_child(bridge_obj, "Subscription");
    amxd_object_for_each(instance, it, sub_obj) {
        amxd_object_t* instance = amxc_container_of(it, amxd_object_t, it);
        update_param_string(instance, "Status", "Disabled");
    }
    res = true;
exit:
    return res;
}

static bool broker_process_disable_bridges(amxd_object_t* object) {
    bool res = false;
    when_null(object, exit);
    amxd_object_t* bridge_obj = amxd_object_get_child(object, "Bridge");
    amxd_object_for_each(instance, it, bridge_obj) {
        amxd_object_t* instance = amxc_container_of(it, amxd_object_t, it);
        update_param_string(instance, "Status", "Disabled");
        broker_process_disable_subscriptions(instance);
    }
    res = true;
exit:
    return res;
}

static bool broker_process_bridge_status(amxd_object_t* object, const char* bridge_log, const char* status) {
    bool res = false;
    amxd_object_t* bridges = NULL;
    amxc_string_t bridge_string;

    when_null(object, exit);
    bridges = amxd_object_get_child(object, "Bridge");
    amxc_string_init(&bridge_string, 0);
    amxc_string_append(&bridge_string, bridge_log, strlen(bridge_log));

    amxd_object_for_each(instance, it, bridges) {
        amxd_object_t* instance = amxc_container_of(it, amxd_object_t, it);
        uint32_t enable = amxd_object_get_value(uint32_t, instance, "Enable", NULL);
        if(enable) {
            char* name = amxd_object_get_value(cstring_t, instance, "Name", NULL);
            char* clientId = amxd_object_get_value(cstring_t, instance, "ClientId", NULL);
            if((amxc_string_search(&bridge_string, name, 0) != -1) || (amxc_string_search(&bridge_string, clientId, 0) != -1)) {
                update_param_string(instance, "Status", status);
            }
            free(name);
            free(clientId);
        }
    }
    res = true;
    amxc_string_clean(&bridge_string);
exit:
    return res;
}

static void broker_parse_logs(const char* output) {
    amxc_string_t log_string;
    amxd_object_t* brokers = amxd_dm_findf(mqttbroker_get_dm(), "MQTTBroker.Broker.");

    if(STRING_EMPTY(output)) {
        return;
    }

    amxc_string_init(&log_string, 0);
    amxc_string_append(&log_string, output, strlen(output));

    if(amxc_string_search(&log_string, "Error:", 0) != -1) {
        SAH_TRACEZ_ERROR(ME, "%s", output);
    } else if(amxc_string_search(&log_string, "Warning:", 0) != -1) {
        SAH_TRACEZ_WARNING(ME, "%s", output);
    } else {
        SAH_TRACEZ_INFO(ME, "%s", output);
    }

    if((amxc_string_search(&log_string, "Bridge", 0) != -1) && (amxc_string_search(&log_string, "sending CONNECT", 0) != -1)) {
        amxd_object_for_each(instance, it, brokers) {
            amxd_object_t* instance = amxc_container_of(it, amxd_object_t, it);
            uint32_t enable = amxd_object_get_value(uint32_t, instance, "Enable", NULL);
            if(enable) {
                broker_process_bridge_status(instance, amxc_string_get(&log_string, 0), "Connecting");
            }
        }
    }

    if(amxc_string_search(&log_string, "Error creating bridge: Broken pipe.", 0) != -1) {
        amxd_object_for_each(instance, it, brokers) {
            amxd_object_t* instance = amxc_container_of(it, amxd_object_t, it);
            uint32_t enable = amxd_object_get_value(uint32_t, instance, "Enable", NULL);
            char* status = amxd_object_get_value(cstring_t, instance, "Status", NULL);
            if(enable && (strcmp(status, "Connecting") == 0) && (strcmp(status, "Error_BrokerUnreachable") != 0)) {
                broker_process_bridge_status(instance, amxc_string_get(&log_string, 0), "Error_BrokerUnreachable");
            }
            free(status);
        }
    }

    if(amxc_string_search(&log_string, "Received SUBACK from ", 0) != -1) {
        amxd_object_for_each(instance, it, brokers) {
            amxd_object_t* instance = amxc_container_of(it, amxd_object_t, it);
            uint32_t enable = amxd_object_get_value(uint32_t, instance, "Enable", NULL);
            if(enable) {
                broker_process_bridge_status(instance, amxc_string_get(&log_string, 0), "Connected");
            }
        }
    }

    amxc_string_clean(&log_string);
}

static void broker_read_subproc(int fdesc, UNUSED void* priv) {
    char* line = NULL;
    size_t len = 0;
    FILE* fp = NULL;
    int _fd = dup(fdesc);
    fp = fdopen(_fd, "r");

    when_null_trace(fp, exit, ERROR, "unable to open mosquitto log: %s", strerror(errno));

    while((getline(&line, &len, fp)) != -1) {
        broker_parse_logs(line);
    }

    free(line);
    fclose(fp);
exit:
    return;
}

bool broker_process_stop(amxd_object_t* object) {
    amxd_object_t* brokers = amxd_dm_findf(mqttbroker_get_dm(), "MQTTBroker.Broker.");
    amxp_subproc_t* process = NULL;
    bool ret = true;
    int retry = 5;

    amxd_object_for_each(instance, it, brokers) {
        amxd_object_t* instance = amxc_container_of(it, amxd_object_t, it);
        uint32_t pid = 0;

        if(is_listener(object, instance)) {
            pid = amxd_object_get_value(uint32_t, instance, "PID", NULL);
            if(pid == 0) {
                continue;
            }

            process = amxp_subproc_find(pid);
            when_null(process, exit);

            amxo_connection_remove(mqttbroker_get_parser(), get_fd_by_pid(pid, "stdout"));
            amxo_connection_remove(mqttbroker_get_parser(), get_fd_by_pid(pid, "stderr"));
            delete_broker_process_by_pid(pid);

            while(retry && amxp_subproc_is_running(process)) {
                SAH_TRACEZ_NOTICE(ME, "Stopping Mosquitto process");

                amxp_subproc_kill(process, SIGKILL);
                if(!amxp_subproc_wait(process, 1000)) {
                }
                retry--;
            }

            if(amxp_subproc_is_running(process)) {
                SAH_TRACEZ_ERROR(ME, "Failed to stop Mosquitto process.");
                ret = false;
            } else {
                amxp_slot_disconnect(amxp_subproc_get_sigmngr(process), "stop", termination_callback);
                termination_callback = NULL;
                amxp_subproc_delete(&process);

                if(!broker_process_unsetFW(object)) {
                    SAH_TRACEZ_WARNING(ME, "Failed to unset firewall");
                }
            }
exit:
            if(ret) {
                broker_clean_files(instance);
                update_param_string(instance, "Status", "Disabled");
                broker_process_disable_bridges(instance);
            }
        }
    }
    return ret;
}

static bool broker_process_go(amxd_object_t* object) {
    amxp_subproc_t* process_mqttbroker = NULL;
    amxd_object_t* brokers = amxd_dm_findf(mqttbroker_get_dm(), "MQTTBroker.Broker.");
    process_t* process = NULL;
    int32_t fd_stdout = -1;
    int32_t fd_stderr = -1;
    int pid = 0;
    bool ret = false;

    char* confFile = NULL;

    if(amxp_subproc_new(&process_mqttbroker) != 0) {
        SAH_TRACEZ_ERROR(ME, "Failed to create process");
        return false;
    }

    amxp_slot_connect(amxp_subproc_get_sigmngr(process_mqttbroker), "stop", NULL, slot_proc_stop, NULL);

    fd_stdout = amxp_subproc_open_fd(process_mqttbroker, STDOUT_FILENO);
    fd_stderr = amxp_subproc_open_fd(process_mqttbroker, STDERR_FILENO);

    if((fd_stdout == -1) || (fd_stderr == -1)) {
        SAH_TRACEZ_ERROR(ME, "Failed to open file descriptor.");
        goto exit;
    }

    if(fcntl(fd_stdout, F_SETFL, fcntl(fd_stdout, F_GETFL, 0) | O_NONBLOCK) < 0) {
        SAH_TRACEZ_ERROR(ME, "Failed to make stdout non blocking");
        goto exit;
    }

    if(fcntl(fd_stderr, F_SETFL, fcntl(fd_stderr, F_GETFL, 0) | O_NONBLOCK) < 0) {
        SAH_TRACEZ_ERROR(ME, "Failed to make stderr non blocking");
        goto exit;
    }

    if(amxo_connection_add(mqttbroker_get_parser(), fd_stdout, broker_read_subproc, NULL, AMXO_CUSTOM, NULL) != 0) {
        SAH_TRACEZ_ERROR(ME, "amxo_connection_add failed for stdout");
    }

    if(amxo_connection_add(mqttbroker_get_parser(), fd_stderr, broker_read_subproc, NULL, AMXO_CUSTOM, NULL) != 0) {
        SAH_TRACEZ_ERROR(ME, "amxo_connection_add failed for stderr");
    }

    confFile = amxd_object_get_value(cstring_t, object, "ConfFile", NULL);
    bool ok_process = amxp_subproc_start(process_mqttbroker, (char*) "mosquitto", "-c", confFile, NULL);
    free(confFile);

    if(ok_process) {
        SAH_TRACEZ_ERROR(ME, "Failed to fork the process");
        goto exit;
    }

    pid = (int) amxp_subproc_get_pid(process_mqttbroker);
    when_false_trace(pid > 0, exit, ERROR, "Failed to get PID of the process.");

    if(amxp_subproc_is_running(process_mqttbroker)) {
        ret = true;
    }

    process = create_broker_process(pid, fd_stdout, fd_stderr);
    if(!process) {
        SAH_TRACEZ_ERROR(ME, "Failed to create process entry.");
    }

exit:
    amxd_object_for_each(instance, it, brokers) {
        amxd_object_t* instance = amxc_container_of(it, amxd_object_t, it);
        bool enable = amxd_object_get_value(bool, instance, "Enable", NULL);
        if((enable) && (is_listener(object, instance))) {
            if(ret) {
                update_param_uint32(instance, "PID", pid);
                update_param_string(instance, "Status", "Enabled");
            } else {
                update_param_string(instance, "Status", "Error");
            }
        }
    }
    if(!ret) {
        if(amxp_subproc_is_running(process_mqttbroker)) {
            broker_process_stop(object);
        }
        amxp_subproc_delete(&process_mqttbroker);
    }

    return ret;
}

bool broker_process_start(amxd_object_t* object) {
    SAH_TRACEZ_NOTICE(ME, "Starting Mosquitto process");
    uint32_t pid = amxd_object_get_value(uint32_t, object, "PID", NULL);

    if(pid) {
        SAH_TRACEZ_INFO(ME, "Broker already running");
        return true;
    }

    if(!broker_process_checkDir(MOSQUITTO_CONF_PATH)) {
        SAH_TRACEZ_ERROR(ME, "Configuration directory failure '%s'", MOSQUITTO_CONF_PATH);
        return false;
    }

    if(check_if_conf_file_exists(object)) {
        if(unlink(amxd_object_get_value(cstring_t, object, "ConfFile", NULL)) != 0) {
            SAH_TRACEZ_ERROR(ME, "Unable to remove mosquitto configuration");
        }
    }

    if(!broker_process_writeConfig(object)) {
        SAH_TRACEZ_ERROR(ME, "Failed to write mosquitto configuration");
        return false;
    }

    if(!broker_process_setFW(object)) {
        SAH_TRACEZ_ERROR(ME, "Failed to set firewall");
        return false;
    }

    if(!broker_process_go(object)) {
        SAH_TRACEZ_ERROR(ME, "Failed to start Mosquitto process");
        return false;
    }

    SAH_TRACEZ_INFO(ME, "Started mosquitto process");
    return true;
}
