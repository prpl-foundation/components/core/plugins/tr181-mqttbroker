/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2023 SoftAtHome
**
** Redistribution and use in source and binary forms, with or without modification,
** are permitted provided that the following conditions are met:
**
** 1. Redistributions of source code must retain the above copyright notice,
** this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above copyright notice,
** this list of conditions and the following disclaimer in the documentation
** and/or other materials provided with the distribution.
**
** Subject to the terms and conditions of this license, each copyright holder
** and contributor hereby grants to those receiving rights under this license
** a perpetual, worldwide, non-exclusive, no-charge, royalty-free, irrevocable
** (except for failure to satisfy the conditions of this license) patent license
** to make, have made, use, offer to sell, sell, import, and otherwise transfer
** this software, where such license applies only to those patent claims, already
** acquired or hereafter acquired, licensable by such copyright holder or contributor
** that are necessarily infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright holders and
** non-copyrightable additions of contributors, in source or binary form) alone;
** or
**
** (b) combination of their Contribution(s) with the work of authorship to which
** such Contribution(s) was added by such copyright holder or contributor, if,
** at the time the Contribution is added, such addition causes such combination
** to be necessarily infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any copyright
** holder or contributor is granted under this license, whether expressly, by
** implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
** AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
** IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
** ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
** LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
** DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
** SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
** CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
** OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
** USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <math.h>
#include <debug/sahtrace.h>
#include "mqttbroker.h"
#include "mosquitto_proc.h"
#include "dm_mqttbroker.h"
#include "interface.h"

bool mqttbroker_all_interfaces_up(amxd_object_t* object) {
    bool ret = false;
    amxd_object_t* brokers = amxd_dm_findf(mqttbroker_get_dm(), "MQTTBroker.Broker.");
    char* alias = amxd_object_get_value(cstring_t, object, "Alias", NULL);
    SAH_TRACEZ_INFO(ME, "Checking if all interfaces of instances related to broker %s are up:", alias);
    free(alias);

    when_null(object, exit);
    when_null(brokers, exit);

    amxd_object_for_each(instance, it, brokers) {
        amxd_object_t* instance = amxc_container_of(it, amxd_object_t, it);
        mqtt_broker_t* broker = (mqtt_broker_t*) instance->priv;
        bool enable = amxd_object_get_value(bool, instance, "Enable", NULL);

        when_null(broker, exit);
        if((enable) && (is_listener(object, instance))) {
            if(broker->interface.isup == false) {
                goto exit;
            }
        }
    }

    ret = true;
exit:
    return ret;
}

void mqttbroker_interface_query_done(bool enable, amxd_object_t* object) {
    if(enable) {
        if(mqttbroker_all_interfaces_up(object)) {
            mqtt_broker_enable(object);
        }
    } else {
        char* status = amxd_object_get_value(cstring_t, object, "Status", NULL);
        if(status) {
            if(strcmp(status, "Enabled") == 0) {
                mqtt_broker_intf_down(object);
            }
        }
        free(status);
    }
}

static void mqttbroker_interface_query_isup_cb(UNUSED const char* sig_name,
                                               const amxc_var_t* data,
                                               void* priv) {
    amxd_object_t* object = NULL;
    mqtt_broker_t* broker = NULL;
    bool isup = false;

    when_null(data, exit);
    when_null(priv, exit);

    object = (amxd_object_t*) priv;
    broker = (mqtt_broker_t*) object->priv;
    when_null(broker, exit);

    isup = amxc_var_dyncast(bool, data);

    if(isup) {
        SAH_TRACEZ_INFO(ME, "Interface %s of mqtt broker is up", broker->interface.name);
        broker->interface.isup = true;
    } else {
        SAH_TRACEZ_ERROR(ME, "Interface %s of mqtt broker is down, disconnect", broker->interface.name);
        broker->interface.isup = false;
    }

    mqttbroker_interface_query_done(isup, object);

exit:
    return;
}

static int mqttbroker_interface_open_queries(amxd_object_t* object) {
    int retval = -1;
    mqtt_broker_t* broker = NULL;

    when_null(object, exit);
    broker = (mqtt_broker_t*) object->priv;
    when_null(broker, exit);

    if(!broker->interface.query_isup) {
        SAH_TRACEZ_INFO(ME, "Open queries for broker interface %s", broker->interface.name);
        broker->interface.query_isup = netmodel_openQuery_isUp(broker->interface.name,
                                                               ME,
                                                               "ipv4-up || ( ipv6-up && global)",
                                                               "down",
                                                               mqttbroker_interface_query_isup_cb,
                                                               (void*) object);
        when_null(broker->interface.query_isup, exit);
    }
    retval = 0;
exit:
    return retval;
}

static void mqttbroker_interface_close_queries(amxd_object_t* object) {
    mqtt_broker_t* broker = NULL;

    when_null(object, exit);
    broker = (mqtt_broker_t*) object->priv;
    when_null(broker, exit);
    SAH_TRACEZ_INFO(ME, "Close queries for broker interface %s", broker->interface.name);
    netmodel_closeQuery(broker->interface.query_isup);
    broker->interface.query_isup = NULL;
exit:
    return;
}

int mqttbroker_interface_init(amxd_object_t* object, const char* name) {
    int retval = -1;
    mqtt_broker_t* broker = NULL;

    when_null(object, exit);
    when_str_empty(name, exit);
    broker = (mqtt_broker_t*) object->priv;
    when_null(broker, exit);
    memset(&broker->interface, 0, sizeof(mqttbroker_interface_t));
    broker->interface.name = strdup(name);
    retval = mqttbroker_interface_open_queries(object);
    when_failed(retval, exit);
    retval = 0;
exit:
    return retval;
}

void mqttbroker_interface_clean(amxd_object_t* object) {
    mqtt_broker_t* broker = NULL;

    when_null(object, exit);
    broker = (mqtt_broker_t*) object->priv;
    when_null(broker, exit);
    if(broker->interface.name) {
        mqttbroker_interface_close_queries(object);
        free(broker->interface.name);
        broker->interface.name = NULL;
    }
    broker->interface.isup = false;
exit:
    return;
}
