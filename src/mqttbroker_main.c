/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2023 SoftAtHome
**
** Redistribution and use in source and binary forms, with or without modification,
** are permitted provided that the following conditions are met:
**
** 1. Redistributions of source code must retain the above copyright notice,
** this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above copyright notice,
** this list of conditions and the following disclaimer in the documentation
** and/or other materials provided with the distribution.
**
** Subject to the terms and conditions of this license, each copyright holder
** and contributor hereby grants to those receiving rights under this license
** a perpetual, worldwide, non-exclusive, no-charge, royalty-free, irrevocable
** (except for failure to satisfy the conditions of this license) patent license
** to make, have made, use, offer to sell, sell, import, and otherwise transfer
** this software, where such license applies only to those patent claims, already
** acquired or hereafter acquired, licensable by such copyright holder or contributor
** that are necessarily infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright holders and
** non-copyrightable additions of contributors, in source or binary form) alone;
** or
**
** (b) combination of their Contribution(s) with the work of authorship to which
** such Contribution(s) was added by such copyright holder or contributor, if,
** at the time the Contribution is added, such addition causes such combination
** to be necessarily infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any copyright
** holder or contributor is granted under this license, whether expressly, by
** implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
** AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
** IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
** ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
** LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
** DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
** SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
** CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
** OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
** USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include <stdlib.h>
#include <stdio.h>
#include <time.h>
#include <string.h>
#include <pthread.h>
#include <sys/stat.h>
#include <sys/types.h>

#include <debug/sahtrace.h>
#include "mqttbroker.h"
#include "dm_mqttbroker.h"
#include "firewall.h"
#include "interface.h"
#include "mosquitto_proc.h"

static mqttbroker_app_t app;

static void mqttbroker_init(amxd_dm_t* dm, amxo_parser_t* parser) {
    SAH_TRACEZ_INFO(ME, "**************************************");
    SAH_TRACEZ_INFO(ME, "*         MQTTBroker started         *");
    SAH_TRACEZ_INFO(ME, "**************************************");
    app.dm = dm;
    app.parser = parser;
    netmodel_initialize();
    load_mod_firewall(parser);
    srand((unsigned int) time(NULL));
    mqtt_init_enable();
}

static void mqttbroker_exit(UNUSED amxd_dm_t* dm, UNUSED amxo_parser_t* parser) {
    netmodel_cleanup();
    process_cleanup();
    app.dm = NULL;
    app.parser = NULL;
}

amxd_dm_t* PRIVATE mqttbroker_get_dm(void) {
    return app.dm;
}

amxo_parser_t* mqttbroker_get_parser(void) {
    return app.parser;
}

int _mqttbroker_main(int reason,
                     amxd_dm_t* dm,
                     amxo_parser_t* parser) {
    SAH_TRACEZ_INFO(ME, "mqttbroker_main, reason: %d", reason);
    switch(reason) {
    case 0:     // START
        mqttbroker_init(dm, parser);
        break;
    case 1:     // STOP
        mqttbroker_exit(dm, parser);
        break;
    default:
        break;
    }
    return 0;
}
