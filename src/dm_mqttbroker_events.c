/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2023 SoftAtHome
**
** Redistribution and use in source and binary forms, with or without modification,
** are permitted provided that the following conditions are met:
**
** 1. Redistributions of source code must retain the above copyright notice,
** this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above copyright notice,
** this list of conditions and the following disclaimer in the documentation
** and/or other materials provided with the distribution.
**
** Subject to the terms and conditions of this license, each copyright holder
** and contributor hereby grants to those receiving rights under this license
** a perpetual, worldwide, non-exclusive, no-charge, royalty-free, irrevocable
** (except for failure to satisfy the conditions of this license) patent license
** to make, have made, use, offer to sell, sell, import, and otherwise transfer
** this software, where such license applies only to those patent claims, already
** acquired or hereafter acquired, licensable by such copyright holder or contributor
** that are necessarily infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright holders and
** non-copyrightable additions of contributors, in source or binary form) alone;
** or
**
** (b) combination of their Contribution(s) with the work of authorship to which
** such Contribution(s) was added by such copyright holder or contributor, if,
** at the time the Contribution is added, such addition causes such combination
** to be necessarily infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any copyright
** holder or contributor is granted under this license, whether expressly, by
** implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
** AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
** IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
** ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
** LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
** DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
** SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
** CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
** OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
** USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <math.h>
#include <debug/sahtrace.h>
#include <debug/sahtrace_macros.h>
#include "mqttbroker.h"
#include "mosquitto_proc.h"
#include "dm_mqttbroker.h"
#include "interface.h"

const char* get_vendor_prefix(void) {
    amxc_var_t* setting = amxo_parser_get_config(mqttbroker_get_parser(), "prefix_");
    const char* prefix = amxc_var_constcast(cstring_t, setting);
    return (prefix != NULL) ? prefix : "";
}

uint32_t get_BrokerID(amxd_object_t* object) {
    uint32_t brokerID = 0;
    amxc_string_t templ_str;
    amxc_string_init(&templ_str, 0);
    amxc_string_setf(&templ_str, "%sBrokerID", get_vendor_prefix());
    brokerID = amxd_object_get_value(uint32_t, object, amxc_string_get(&templ_str, 0), NULL);
    amxc_string_clean(&templ_str);
    return brokerID;
}

bool is_listener(amxd_object_t* object, amxd_object_t* instance) {
    uint32_t brokerID = 0;
    uint32_t instanceBrokerID = 0;
    brokerID = get_BrokerID(object);
    instanceBrokerID = get_BrokerID(instance);
    char* alias = amxd_object_get_value(cstring_t, object, "Alias", NULL);
    char* alias_instance = amxd_object_get_value(cstring_t, object, "Alias", NULL);
    bool ret = false;

    if((brokerID == instanceBrokerID)) {
        SAH_TRACEZ_INFO(ME, "%s and %s found to be instances of the same broker.", alias_instance, alias);
        ret = true;
    }

    free(alias);
    free(alias_instance);
    return ret;
}

void update_param_string(amxd_object_t* object, const char* param, const char* value) {
    amxc_var_t params;
    amxd_trans_t trans;

    amxd_trans_init(&trans);
    amxc_var_init(&params);

    amxd_trans_set_attr(&trans, amxd_tattr_change_ro, true);
    amxd_trans_select_object(&trans, object);
    amxd_trans_set_value(cstring_t, &trans, param, value);
    amxd_trans_apply(&trans, mqttbroker_get_dm());

    amxd_trans_clean(&trans);
    amxc_var_clean(&params);
}

void update_param_uint32(amxd_object_t* object, const char* param, uint32_t value) {
    amxc_var_t params;
    amxd_trans_t trans;

    amxd_trans_init(&trans);
    amxc_var_init(&params);

    amxd_trans_set_attr(&trans, amxd_tattr_change_ro, true);
    amxd_trans_select_object(&trans, object);
    amxd_trans_set_value(uint32_t, &trans, param, value);
    amxd_trans_apply(&trans, mqttbroker_get_dm());

    amxd_trans_clean(&trans);
    amxc_var_clean(&params);
}

void mqtt_init_enable(void) {
    amxd_object_t* brokers = amxd_dm_findf(mqttbroker_get_dm(), "MQTTBroker.Broker.");
    bool enable = false;

    amxd_object_for_each(instance, it, brokers) {
        amxd_object_t* instance = amxc_container_of(it, amxd_object_t, it);
        enable = amxd_object_get_value(bool, instance, "Enable", NULL);
        if(enable) {
            mqtt_broker_update(instance);
        }
    }
}

void mqtt_broker_enable(amxd_object_t* object) {
    char* status = NULL;

    SAH_TRACEZ_INFO(ME, "mqtt broker enable");

    when_null(object, exit);

    status = amxd_object_get_value(cstring_t, object, "Status", NULL);
    when_null(status, exit);

    if(strcmp(status, "Enabled") == 0) {
        goto exit;
    }

    broker_process_start(object);
exit:
    free(status);
    return;
}

void mqtt_broker_disable(amxd_object_t* object) {
    char* status = NULL;
    amxd_object_t* brokers = amxd_dm_findf(mqttbroker_get_dm(), "MQTTBroker.Broker.");
    char* alias = amxd_object_get_value(cstring_t, object, "Alias", NULL);

    SAH_TRACEZ_INFO(ME, "Disable instance %s", alias);

    when_null(object, exit);
    when_null(brokers, exit);

    status = amxd_object_get_value(cstring_t, object, "Status", NULL);
    when_null(status, exit);

    broker_process_stop(object);

exit:
    amxd_object_for_each(instance, it, brokers) {
        amxd_object_t* instance = amxc_container_of(it, amxd_object_t, it);
        bool enable = amxd_object_get_value(bool, instance, "Enable", NULL);
        if((enable) && (is_listener(object, instance))) {
            mqttbroker_interface_clean(instance);
        }
    }

    free(status);
    free(alias);
    return;
}

void mqtt_broker_intf_down(amxd_object_t* object) {
    char* status = NULL;
    status = amxd_object_get_value(cstring_t, object, "Status", NULL);
    when_null(object, exit);
    when_null(status, exit);

    if(strcmp(status, "Disabled") == 0) {
        goto exit;
    }

    broker_process_stop(object);
exit:
    free(status);
    return;
}

static int mqtt_broker_start(UNUSED amxd_object_t* brokers,
                             amxd_object_t* broker,
                             UNUSED void* priv) {
    mqtt_broker_update(broker);
    return 0;
}

void _mqtt_broker_start(UNUSED const char* const sig_name,
                        UNUSED const amxc_var_t* const data,
                        UNUSED void* const priv) {
    amxd_object_t* brokers = amxd_dm_findf(mqttbroker_get_dm(), "MQTTBroker.Broker.");

    SAH_TRACEZ_INFO(ME, "Start Brokers");
    amxd_object_for_all(brokers, "[Enable == true].", mqtt_broker_start, NULL);
}

void _mqtt_broker_added(UNUSED const char* const sig_name,
                        const amxc_var_t* const data,
                        UNUSED void* const priv) {
    amxd_dm_t* dm = mqttbroker_get_dm();
    amxd_object_t* object = amxd_dm_signal_get_object(dm, data);

    object = amxd_object_get_instance(object, NULL, GET_UINT32(data, "index"));
    mqtt_broker_update(object);
}

void _mqtt_broker_removed(UNUSED const char* const sig_name,
                          const amxc_var_t* const data,
                          UNUSED void* const priv) {
    amxc_string_t templ_str;
    uint32_t brokerID = 0;
    const char* status = GETP_CHAR(data, "parameters.Status");
    amxd_object_t* brokers = amxd_dm_findf(mqttbroker_get_dm(), "MQTTBroker.Broker.");

    if(strcmp(status, "Enabled") != 0) {
        return;
    }

    amxc_string_init(&templ_str, 0);
    amxc_string_setf(&templ_str, "parameters.%sBrokerID", get_vendor_prefix());
    brokerID = GETP_UINT32(data, amxc_string_get(&templ_str, 0));
    amxc_string_clean(&templ_str);

    amxd_object_for_each(instance, it, brokers) {
        amxd_object_t* instance = amxc_container_of(it, amxd_object_t, it);
        bool enable = amxd_object_get_value(bool, instance, "Enable", NULL);
        uint32_t instBrokerID = get_BrokerID(instance);
        if((enable) && (brokerID == instBrokerID)) {
            mqtt_broker_update(instance);
            break;
        }
    }
}

void _mqtt_broker_toggled(UNUSED const char* const sig_name,
                          const amxc_var_t* const data,
                          UNUSED void* const priv) {
    amxd_dm_t* dm = mqttbroker_get_dm();
    amxd_object_t* object = amxd_dm_signal_get_object(dm, data);
    bool enabled = GETP_BOOL(data, "parameters.Enable.to");
    if(enabled) {
        mqtt_broker_disable(object);
        mqtt_broker_update(object);
    } else {
        char* status = amxd_object_get_value(cstring_t, object, "Status", NULL);

        if(status) {
            if(strcmp(status, "Disabled") != 0) {
                mqtt_broker_disable(object);
                mqtt_broker_update(object); // keep other listeners
            }
        }

        free(status);
    }
}

void _mqtt_broker_updated(UNUSED const char* const sig_name,
                          const amxc_var_t* const data,
                          UNUSED void* const priv) {
    amxd_dm_t* dm = mqttbroker_get_dm();
    amxd_object_t* object = amxd_dm_signal_get_object(dm, data);
    char* status = amxd_object_get_value(cstring_t, object, "Status", NULL);

    if(status) {
        if(strcmp(status, "Disabled") != 0) {
            mqtt_broker_disable(object);
            mqtt_broker_update(object);
        }
    }
    free(status);
}


void _mqtt_broker_user_changed(UNUSED const char* const sig_name,
                               const amxc_var_t* const data,
                               UNUSED void* const priv) {
    amxd_dm_t* dm = mqttbroker_get_dm();
    amxd_object_t* object = amxd_dm_signal_get_object(dm, data);
    amxd_object_t* parent_obj = amxd_object_get_parent(amxd_object_get_parent(object));
    bool enable = amxd_object_get_value(bool, parent_obj, "Enable", NULL);
    char* username = amxd_object_get_value(cstring_t, object, "Username", NULL);
    char* password = amxd_object_get_value(cstring_t, object, "Password", NULL);

    if((enable) && (!STRING_EMPTY(username)) && (!STRING_EMPTY(password))) {
        mqtt_broker_disable(parent_obj);
        mqtt_broker_update(parent_obj);
    }

    free(username);
    free(password);
}

void _mqtt_broker_auth_changed(UNUSED const char* const sig_name,
                               const amxc_var_t* const data,
                               UNUSED void* const priv) {
    amxd_dm_t* dm = mqttbroker_get_dm();
    amxd_object_t* object = amxd_dm_signal_get_object(dm, data);
    object = amxd_object_get_instance(object, NULL, GET_UINT32(data, "index"));
    amxd_object_t* parent_obj = amxd_object_get_parent(object);
    bool enable = amxd_object_get_value(bool, parent_obj, "Enable", NULL);
    char* username = amxd_object_get_value(cstring_t, object, "Username", NULL);
    char* password = amxd_object_get_value(cstring_t, object, "Password", NULL);

    if(enable && (!STRING_EMPTY(username)) && (!STRING_EMPTY(password))) {
        mqtt_broker_disable(parent_obj);
        mqtt_broker_update(parent_obj);
    }

    free(username);
    free(password);
}

void _mqtt_broker_bridge_changed(UNUSED const char* const sig_name,
                                 const amxc_var_t* const data,
                                 UNUSED void* const priv) {
    amxd_dm_t* dm = mqttbroker_get_dm();
    amxd_object_t* bridge_object = amxd_dm_signal_get_object(dm, data);
    amxd_object_t* parent_obj = amxd_object_get_parent(amxd_object_get_parent(bridge_object));
    bool broker_enable = amxd_object_get_value(bool, parent_obj, "Enable", NULL);
    bool bridge_enable = amxd_object_get_value(bool, bridge_object, "Enable", NULL);

    if(broker_enable && bridge_enable) {
        mqtt_broker_disable(parent_obj);
        mqtt_broker_update(parent_obj);
    }
}

void _mqtt_broker_bridge_sub_changed(UNUSED const char* const sig_name,
                                     const amxc_var_t* const data,
                                     UNUSED void* const priv) {
    amxd_dm_t* dm = mqttbroker_get_dm();
    amxd_object_t* bridge_sub_object = amxd_dm_signal_get_object(dm, data);
    amxd_object_t* bridge_obj = amxd_object_get_parent(amxd_object_get_parent(bridge_sub_object));
    amxd_object_t* parent_obj = amxd_object_get_parent(amxd_object_get_parent(amxd_object_get_parent(amxd_object_get_parent(bridge_sub_object))));
    bool broker_enable = amxd_object_get_value(bool, parent_obj, "Enable", NULL);
    bool bridge_enable = amxd_object_get_value(bool, bridge_obj, "Enable", NULL);
    bool bridge_sub_enable = amxd_object_get_value(bool, bridge_sub_object, "Enable", NULL);

    if(broker_enable && bridge_enable && bridge_sub_enable) {
        mqtt_broker_disable(parent_obj);
        mqtt_broker_update(parent_obj);
    }
}

void _mqtt_broker_bridge_server_changed(UNUSED const char* const sig_name,
                                        const amxc_var_t* const data,
                                        UNUSED void* const priv) {
    amxd_dm_t* dm = mqttbroker_get_dm();
    amxd_object_t* bridge_server_object = amxd_dm_signal_get_object(dm, data);
    amxd_object_t* bridge_obj = amxd_object_get_parent(amxd_object_get_parent(bridge_server_object));
    amxd_object_t* parent_obj = amxd_object_get_parent(amxd_object_get_parent(amxd_object_get_parent(amxd_object_get_parent(bridge_server_object))));
    bool broker_enable = amxd_object_get_value(bool, parent_obj, "Enable", NULL);
    bool bridge_enable = amxd_object_get_value(bool, bridge_obj, "Enable", NULL);
    bool bridge_server_enable = amxd_object_get_value(bool, bridge_server_object, "Enable", NULL);

    if(broker_enable && bridge_enable && bridge_server_enable) {
        mqtt_broker_disable(parent_obj);
        mqtt_broker_update(parent_obj);
    }
}

void _mqtt_broker_acl_changed(UNUSED const char* const sig_name,
                              const amxc_var_t* const data,
                              UNUSED void* const priv) {
    amxd_dm_t* dm = mqttbroker_get_dm();
    amxd_object_t* object = amxd_dm_signal_get_object(dm, data);
    amxd_object_t* parent_obj = amxd_object_get_parent(amxd_object_get_parent(amxd_object_get_parent(amxd_object_get_parent(object))));
    bool enable = amxd_object_get_value(bool, parent_obj, "Enable", NULL);
    char* topic = amxd_object_get_value(cstring_t, object, "Topic", NULL);
    char* access = amxd_object_get_value(cstring_t, object, "AccessRights", NULL);

    if(enable && (!STRING_EMPTY(topic)) && (!STRING_EMPTY(access))) {
        mqtt_broker_disable(parent_obj);
        mqtt_broker_update(parent_obj);
    }

    free(topic);
    free(access);
}

amxd_status_t _mqtt_broker_cleanup(amxd_object_t* object,
                                   UNUSED amxd_param_t* param,
                                   amxd_action_t reason,
                                   UNUSED const amxc_var_t* const args,
                                   UNUSED amxc_var_t* const retval,
                                   UNUSED void* priv) {
    amxd_status_t status = amxd_status_ok;

    when_null(object, exit);
    if(reason != action_object_destroy) {
        status = amxd_status_function_not_implemented;
        goto exit;
    }

    priv = (mqtt_broker_t*) object->priv;
    if(priv != NULL) {

        mqtt_broker_disable(object);
        free(priv);
        object->priv = NULL;
    }

exit:
    return status;
}


static int mqttbroker_new(amxd_object_t* object,
                          amxc_var_t* params,
                          mqtt_broker_t** broker) {
    int retval = -1;

    when_null(object, exit);
    when_null(params, exit);
    when_null(broker, exit);

    if(object->priv == NULL) {
        *broker = calloc(1, sizeof(mqtt_broker_t));
        object->priv = *broker;
    } else {
        *broker = (mqtt_broker_t*) object->priv;
    }

    retval = 0;
exit:
    return retval;
}

void mqtt_broker_update(amxd_object_t* object) {
    amxd_object_t* brokers = amxd_dm_findf(mqttbroker_get_dm(), "MQTTBroker.Broker.");
    char* status = NULL;

    when_null(object, exit);
    when_null(brokers, exit);

    status = amxd_object_get_value(cstring_t, object, "Status", NULL);
    when_null(status, exit);

    if(strcmp(status, "Enabled") == 0) {
        goto exit;
    }

    amxd_object_for_each(instance, it, brokers) {
        amxd_object_t* instance = amxc_container_of(it, amxd_object_t, it);
        mqtt_broker_t* broker = NULL;
        amxc_var_t params;
        const char* interface = NULL;
        bool enable = amxd_object_get_value(bool, instance, "Enable", NULL);

        if((enable) && (is_listener(object, instance))) {
            amxc_var_init(&params);
            if(mqttbroker_new(instance, &params, &broker) != 0) {
                SAH_TRACEZ_ERROR(ME, "Error creating new mqtt broker");
                update_param_string(instance, "Status", "Error");
                amxc_var_clean(&params);
                goto exit;
            }

            amxd_object_get_params(instance, &params, amxd_dm_access_protected);
            interface = GET_CHAR(&params, "Interface");
            mqttbroker_interface_clean(instance);

            if(!STRING_EMPTY(interface)) {
                mqttbroker_interface_init(instance, interface);
            } else {
                SAH_TRACEZ_ERROR(ME, "Interface is empty");
                update_param_string(instance, "Status", "Error");
            }

            amxc_var_clean(&params);
        }
    }

exit:
    free(status);
}

/**
 * @brief
 *
 * Create and/or configure a broker.
 * If the 'alias' exists, then the server is reconfigured.
 * Otherwise a new server is created and configured.
 * @param alias The broker identification.
 * @param broker The broker configuration
 * @return bool true if the broker is added.
 * @Version V1.0
 */
amxd_status_t _addBroker(amxd_object_t* object,
                         UNUSED amxd_function_t* func,
                         amxc_var_t* args,
                         amxc_var_t* ret) {
    amxd_status_t retval = amxd_status_unknown_error;
    amxd_object_t* child = amxd_object_get_child(object, "Broker");
    amxc_var_t* function_args = NULL;
    amxd_object_t* broker = NULL;
    char* status = NULL;
    const char* alias = GET_CHAR(args, "alias");

    function_args = amxc_var_get_path(args, "broker", AMXC_VAR_FLAG_DEFAULT);

    when_str_empty_trace(alias, exit, ERROR, "No alias given for broker");
    when_null_trace(function_args, exit, ERROR, "No broker parameters present");

    broker = amxd_object_findf(object, "Broker.%s.", alias);

    if(broker) {
        retval = amxd_object_set_params(broker, function_args);
    } else {
        retval = amxd_object_add_instance(&broker, child, alias, 0, function_args);
    }
    when_failed(retval, exit);

    if(amxd_object_get_value(bool, broker, "Enable", NULL)) {
        mqtt_broker_update(broker);
    } else {
        status = amxd_object_get_value(cstring_t, broker, "Status", NULL);
        if(strcmp(status, "Enabled") == 0) {
            mqtt_broker_disable(broker);
        }
        free(status);
    }

    retval = amxd_status_ok;

exit:
    amxc_var_set(uint32_t, ret, retval);
    return retval;
}

/**
 * @brief
 * Delete a broker.
 * @param alias The broker identification.
 * @return bool true if the broker is added.
 * @Version V1.0
 */
amxd_status_t _deleteBroker(amxd_object_t* object,
                            UNUSED amxd_function_t* func,
                            amxc_var_t* args,
                            amxc_var_t* ret) {
    amxd_status_t retval = amxd_status_unknown_error;
    const char* alias = GET_CHAR(args, "alias");
    amxd_object_t* broker = NULL;

    when_str_empty_trace(alias, exit, ERROR, "No alias given for broker.");

    broker = amxd_object_findf(object, "Broker.%s.", alias);
    amxd_object_delete(&broker);

    retval = amxd_status_ok;

exit:
    amxc_var_set(uint32_t, ret, retval);
    return retval;
}

/**
 * @brief
 * Get broker configuration.
 * @param alias The broker identification.
 * @param broker The broker configuration.
 * @return broker variant containing the broker configuration.
 * @Version V1.0
 */
amxd_status_t _getBroker(amxd_object_t* object,
                         UNUSED amxd_function_t* func,
                         amxc_var_t* args,
                         amxc_var_t* ret) {
    amxd_status_t retval = amxd_status_unknown_error;
    amxd_object_t* broker = NULL;
    const char* alias = GET_CHAR(args, "alias");

    when_str_empty_trace(alias, exit, ERROR, "No alias given for broker.");

    broker = amxd_object_findf(object, "Broker.%s", alias);
    when_null(broker, exit);

    amxc_var_init(ret);
    amxc_var_set_type(ret, AMXC_VAR_ID_HTABLE);
    amxd_object_get_params(broker, ret, amxd_dm_access_protected);

    retval = amxd_status_ok;

exit:
    return retval;
}

/**
 * @brief
 *
 * Set the 'Enable' parameter for the passed broker.
 * @param alias The broker identification.
 * @param enable The value to set the 'Enable' parameter.
 * @return bool true if the 'Enable' parameter is set.
 * @Version V1.0
 */
amxd_status_t _setEnable(amxd_object_t* object,
                         UNUSED amxd_function_t* func,
                         amxc_var_t* args,
                         amxc_var_t* ret) {
    amxd_status_t retval = amxd_status_unknown_error;
    amxc_var_t enable_var;
    amxd_object_t* broker = NULL;
    char* status = NULL;
    const char* alias = GET_CHAR(args, "alias");
    const bool enable = GET_BOOL(args, "enable");

    amxc_var_init(&enable_var);
    amxc_var_set(bool, &enable_var, enable);

    when_str_empty_trace(alias, exit, ERROR, "No alias given for broker");

    broker = amxd_object_findf(object, "Broker.%s.", alias);
    retval = amxd_status_object_not_found;
    when_null(broker, exit);

    retval = amxd_object_set_param(broker, "Enable", &enable_var);
    when_failed(retval, exit);

    if(enable) {
        update_param_string(broker, "Status", "Enabled");
    } else {
        status = amxd_object_get_value(cstring_t, broker, "Status", NULL);
        if(strcmp(status, "Enabled") == 0) {
            mqtt_broker_disable(broker);
        }
        free(status);
    }

    retval = amxd_status_ok;

exit:
    amxc_var_clean(&enable_var);
    amxc_var_set(uint32_t, ret, retval);
    return retval;
}
