# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased]


## Release v0.1.0 - 2024-12-16(16:57:48 +0000)

### Other

- [Prpl][SoW2024]MQTT Broker Bridge Functionality

## Release v0.0.37 - 2024-11-25(16:19:58 +0000)

### Other

- [Security][SAHPairing]AllowAnonymous=1 by default in MQTTBroker

## Release v0.0.36 - 2024-10-22(06:41:51 +0000)

### Other

- [MQTTBroker] when changing password very fast some entries are skipped in the passwd file

## Release v0.0.35 - 2024-10-16(15:47:16 +0000)

### Other

- [tr181-mqttbroker][prplOS] MQTT broker fails to enable a disabled broker

## Release v0.0.34 - 2024-09-18(09:41:16 +0000)

### Other

- [mosquitto] - Enable error logs by default

## Release v0.0.33 - 2024-09-10(15:19:47 +0000)

### Other

- Add new user/group for certificates to allow mosquitto to load them when running as non-root

## Release v0.0.32 - 2024-09-10(11:51:16 +0000)

### Other

- [TR181-MQTTBroker] Unable to enable ConnectionMessages Logging for Moquitto Broker

## Release v0.0.31 - 2024-08-30(09:46:59 +0000)

### Other

- Add new user/group for certificates to allow mosquitto to load them when running as non-root

## Release v0.0.30 - 2024-08-01(10:01:12 +0000)

### Other

- MQTT broker lacked topic level authorization controls

## Release v0.0.29 - 2024-07-23(07:53:32 +0000)

### Fixes

- Better shutdown script

## Release v0.0.28 - 2024-07-18(15:50:00 +0000)

## Release v0.0.27 - 2024-06-19(12:51:42 +0000)

### Other

- make availble on gitlab.com

## Release v0.0.26 - 2024-06-10(12:42:39 +0000)

### Other

- [TR181-MQTTBroker] - Fix logs from mosquitto

## Release v0.0.25 - 2024-05-24(12:02:35 +0000)

### Other

- [MQTTBroker] Allow to start MQTT broker as non-root

## Release v0.0.24 - 2024-05-23(12:56:46 +0000)

### Other

- [MQTTBroker] Allow to start MQTT broker as non-root

## Release v0.0.23 - 2024-05-16(06:53:44 +0000)

### Other

- [TR181-MQTTBroker] - Allow multiple listeners in the same mosquitto process

## Release v0.0.22 - 2024-04-27(08:09:15 +0000)

### Other

- [MQTTBroker] Use interface full path

## Release v0.0.21 - 2024-04-26(11:53:16 +0000)

### Other

- map MQTTBroker to Device.MQTT.Broker

## Release v0.0.20 - 2024-04-25(11:32:21 +0000)

### Other

- [MQTTBroker] - Allow multiple non linux user/password for authentication

## Release v0.0.19 - 2024-04-25(09:47:15 +0000)

### Other

- [MQTTBroker] Add ODL defaults.d directory

## Release v0.0.18 - 2024-04-24(17:56:49 +0000)

### Other

- map MQTTBroker to Device.MQTTBroker

## Release v0.0.17 - 2024-04-24(15:37:06 +0000)

### Other

- [MQTTBroker] Add ODL defaults.d directory

## Release v0.0.16 - 2024-04-10(07:11:10 +0000)

### Changes

- Make amxb timeouts configurable

## Release v0.0.15 - 2024-04-03(16:30:56 +0000)

## Release v0.0.14 - 2024-03-19(20:59:28 +0000)

### Other

- Fix TR181-MQTTBroker Username/Password Authentication

## Release v0.0.13 - 2024-01-23(13:47:09 +0000)

### Fixes

- [PCM] The BackupStatus takes the value "PartialSuccess" instead of "Success"

## Release v0.0.12 - 2023-12-12(13:24:11 +0000)

### Other

- [TR181-MQTTBroker] Sometimes call to set firewall service returns timeout while being OK

## Release v0.0.11 - 2023-12-07(10:31:40 +0000)

### Other

- [TR181-MQTTBroker] Sometimes call to set firewall service returns timeout while being OK

## Release v0.0.10 - 2023-12-05(12:13:30 +0000)

### Other

- [AP Config] AP and HGW are not paired after the upgrade

## Release v0.0.9 - 2023-10-28(07:20:54 +0000)

### Other

- [CHR2fA] Update Licenses in Network components

## Release v0.0.8 - 2023-10-13(14:13:52 +0000)

### Changes

-  All applications using sahtrace logs should use default log levels

## Release v0.0.7 - 2023-09-07(15:01:48 +0000)

### Fixes

- [tr181-pcm] All the plugins that are register to PCM should set the pcm_svc_config

## Release v0.0.6 - 2023-09-07(12:43:36 +0000)

### Other

- Make component available on gitlab.softathome.com

## Release v0.0.5 - 2023-07-04(15:04:52 +0000)

### Fixes

- Init script has no shutdown function

## Release v0.0.4 - 2023-06-19(07:27:30 +0000)

### Fixes

- Invalid odl

## Release v0.0.3 - 2023-06-07(15:29:10 +0000)

### Other

- TR181-MQTTBroker - Add dm param to select between IPv4, IPv6 or both

## Release v0.0.2 - 2023-06-06(09:57:34 +0000)

### Other

- [TR181-MQTTBroker] An amx plugin must be created which supports the MQTTBroker Datamodel

## Release v0.0.1 - 2023-05-24(10:06:11 +0000)

### Other

- [TR181-MQTTBroker] An amx plugin must be created which supports the MQTTBroker Datamodel

